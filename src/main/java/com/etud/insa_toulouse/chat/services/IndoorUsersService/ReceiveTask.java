/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/ReceiveTask.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:47:22 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

import com.etud.insa_toulouse.chat.Message;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.function.Consumer;

public class ReceiveTask implements Runnable {
  private final Socket socket;
  private final Consumer<Message> receiveHandler;

  public ReceiveTask(Socket socket, Consumer<Message> receiveHandler) {
    this.socket = socket;
    this.receiveHandler = receiveHandler;
  }

  public void run() {
    try {
      ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
      Message m = (Message) in.readObject();
      receiveHandler.accept(m);

      socket.close();
    } catch (IOException e) {
      System.err.println("La réception du message a échoué");
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}
