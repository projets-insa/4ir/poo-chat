/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/UserListMessage.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:47:27 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

import com.etud.insa_toulouse.chat.User;
import java.io.Serializable;

public class UserListMessage implements Serializable {
  private User user;
  private int type;

  /**
   * Constructeur
   *
   * @param user L'utilisateur à envoyer dans le message
   * @param type Le type de message :
   *    DISPLAYNAME_ALREADY_TAKEN pour avertir que le displayName est déjà prit
   *    CHANGE_DISPLAYNAME pour demander un changement du displayName
   */
  public UserListMessage(User user, UserListMessageType type) {
    this.user = user;
    this.type = type.toValue();
  }

  /**
   * Getter pour le user
   *
   * @return user
   */
  public User getUser() {
    return user;
  }

  /**
   * Getter pour le type
   *
   * @return type
   */
  public UserListMessageType getType() {
    return UserListMessageType.fromValue(type);
  }

  /**
   * Setter pour le type
   *
   * @param type le nouveau à changer
   */
  public void setType(UserListMessageType type) {
    this.type = type.toValue();
  }
}
