/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/OutdoorUsersService/UpdateMessage.java authors
 * : Clément Chaine (cc.chaine@free.fr) created     : Mar 12 jan 10:04:44 2021
 *
 */

package com.etud.insa_toulouse.chat.services.OutdoorUsersService;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class UpdateMessage implements Serializable {
  private HashMap<String, User> connectedUsers;
  private ArrayList<Message> newMessages;

  public UpdateMessage(HashMap<String, User> connectedUsers, ArrayList<Message> newMessages) {
    this.connectedUsers = connectedUsers;
    this.newMessages = newMessages;
  }

  /**
   * Getter pour les connected users
   *
   * @return connectedUsers
   */
  public HashMap<String, User> getConnectedUsers() {
    return this.connectedUsers;
  }

  /**
   * Getter pour newMessages
   *
   * @return newMessages
   */
  public ArrayList<Message> getNewMessages() {
    return this.newMessages;
  }
}
