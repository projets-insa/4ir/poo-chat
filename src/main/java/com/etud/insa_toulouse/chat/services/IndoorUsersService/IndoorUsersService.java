/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/IndoorUsersService.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:46:56 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.services.CommunicationService;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class IndoorUsersService extends CommunicationService {
  public static final int RECEIVE_PORT = 2945;

  private User us;
  private HashMap<String, User> connectedUsers;

  private UserListServer userListServer;
  private ReceiveServer receiveServer;

  private BiConsumer<CommunicationService, User[]> userChangedHandler;
  private Consumer<Message> receiveHandler;

  /**
   * Constructeur
   *
   * @param userChangedHandler callback lorsque la liste des utilisateurs change
   * @param receiveHandler callback lorsqu'un nouveau message est reçu
   */
  public IndoorUsersService(BiConsumer<CommunicationService, User[]> userChangedHandler,
      Consumer<Message> receiveHandler) {
    connectedUsers = new HashMap<String, User>();

    this.userChangedHandler = userChangedHandler;
    this.receiveHandler = receiveHandler;
  }

  /**
   * Liste l'ensemble des interfaces réseau de la machine
   *
   * @return La liste des interfaces réseau
   */
  public static Enumeration<NetworkInterface> getAllInterfaces() {
    Enumeration<NetworkInterface> netints;
    try {
      netints = NetworkInterface.getNetworkInterfaces();
    } catch (SocketException e) {
      System.err.println("Impossible de récupérer la liste des interfaces");
      netints = null;
    }
    return netints;
  }

  /**
   * Permet à un utilisateur de se connecter
   *
   * @param us l'utilisateur qui nous représent
   * @throws DisplayNameException En cas de nom déjà prit
   */
  public void connect(User us) throws DisplayNameException {
    userListServer = new UserListServer(this, us);
    try {
      this.us = us;
      userListServer.discover();

      // Création d'un serveur tcp de réception des messages
      receiveServer = new ReceiveServer(receiveHandler);
      receiveServer.start();
    } catch (DisplayNameException e) {
      userListServer.close();
      this.us = null;
      throw e;
    }
  }

  /**
   * Permet d'avertir les utilisateurs de notre déconnexion
   */
  public void disconnect() {
    // L'appel à disconnect sur un service déjà déconnecté ne fait rien
    if (this.us != null) {
      userListServer.disconnect();
      userListServer.close();
      receiveServer.kill();
    }
  }

  /**
   * Permet à un utilisateur de changer son displayName
   *
   * @throws DisplayNameException En cas de nom déjà prit
   */
  public void updateDisplayName() throws DisplayNameException {
    try {
      userListServer.updateUser();
    } catch (DisplayNameException e) {
      throw e;
    }
  }

  /**
   * Envoi un message
   *
   * @param message le message à envoyer
   * @throws IOException en cas d'erreur de sérialization
   */
  public void send(Message message) throws IOException {
    // Création d'un socket pour l'envoi
    Socket socket = new Socket(message.getDest().getIp(), RECEIVE_PORT);
    ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

    // Envoi du datagramme
    out.writeObject(message);
    out.flush();

    // Fermeture du socket
    out.close();
    socket.close();
  }

  /**
   * Trouve un utilisateur avec son displayname
   *
   * @param displayName le displayname de l'utilisateur à trouver
   * @return l'utilsateur trouvé ou null
   */
  private User findUser(String displayName) {
    Set<Map.Entry<String, User>> set = connectedUsers.entrySet();
    Iterator<Map.Entry<String, User>> it = set.iterator();
    boolean found = false;
    User u = null;
    while (!found && it.hasNext()) {
      u = it.next().getValue();
      found = u.getDisplayName().equals(displayName);
    }

    if (!found) {
      u = null;
    }

    return u;
  }

  /**
   * Ajoute un utilisateur à la liste des utilisateurs connectés
   *
   * @param user L'utilisateur à ajouter
   * @throws DisplayNameException En cas de nom d'affichage déjà prit
   */
  public void addUser(User user) throws DisplayNameException {
    // On renvoi une exception si le displayName est déjà utilisé
    if (user.getDisplayName().equals(us.getDisplayName())
        || findUser(user.getDisplayName()) != null) {
      throw new DisplayNameException("DISPLAYNAME_ALREADY_TAKEN");
    }
    connectedUsers.put(user.getEmail(), user);

    // On envoi un signal au chatsystem
    userChangedHandler.accept(this, this.getConnectedUsers());
  }

  /**
   * Change le displayName d'un utilisateur dans le hashmap
   * sans détruire l'objet
   *
   * @param user L'utilisateur à modifier avec le nouveau displayName
   * @throws DisplayNameException En cas de nom déjà utilisé
   */
  public void changeRemoteUserDisplayName(User user) throws DisplayNameException {
    // On renvoi une exception si le displayName est déjà utilisé
    if (user.getDisplayName().equals(us.getDisplayName())
        || findUser(user.getDisplayName()) != null) {
      throw new DisplayNameException("DISPLAYNAME_ALREADY_TAKEN");
    }

    // On cherche l'utilisateur qui a cette adresse ip
    User u = connectedUsers.get(user.getEmail());
    if (u != null) {
      // Si on le trouve, il faut le supprimer pour changer la clé
      connectedUsers.remove(u.getDisplayName());

      // On change son displayName
      u.setDisplayName(user.getDisplayName());

      // On le réajoute
      connectedUsers.put(u.getDisplayName(), u);
    }

    // On envoi un signal au chatsystem
    userChangedHandler.accept(this, this.getConnectedUsers());
  }

  /**
   * Supprime un utilisateur
   *
   * @param user l'utilisateur à supprimer
   */
  public void removeUser(User user) throws DisplayNameException {
    connectedUsers.remove(user.getEmail());

    // On envoi un signal au chatsystem
    userChangedHandler.accept(this, this.getConnectedUsers());
  }

  /**
   * Liste l'ensemble des utilisateurs connectés
   *
   * @return La liste des utilisateurs connectés
   */
  public User[] getConnectedUsers() {
    return this.connectedUsers.values().toArray(new User[0]);
  }
}
