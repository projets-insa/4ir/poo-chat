/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/HistoryService/HistoryException.java authors
 * : Clément Chaine (cc.chaine@free.fr) created     : Lun  4 jan 16:07:45 2021
 *
 */

package com.etud.insa_toulouse.chat.services.HistoryService;

public class HistoryException extends Exception {
  /**
   * Constructeur
   *
   * @param message Le message associé à l'exception
   */
  public HistoryException(String message) {
    super(message);
  }
}
