/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/services/OutdoorUsersService/ServletUnreachableException.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Jeu 21 jan 15:08:36 2021
 *
 */

package com.etud.insa_toulouse.chat.services.OutdoorUsersService;

public class ServletUnreachableException extends Exception {
  /**
   * Constructeur
   *
   * @param message Le message associé à l'exception
   */
  public ServletUnreachableException(String message) {
    super(message);
  }
}
