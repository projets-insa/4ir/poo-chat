/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/HistoryService/HistoryService.java authors :
 * Clément Chaine (cc.chaine@free.fr) created     : Mer 30 déc 10:54:42 2020
 *
 */

package com.etud.insa_toulouse.chat.services.HistoryService;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

public class HistoryService {
  private static final String MESSAGE_TABLE_NAME = "MESSAGES";
  private static final String ATTACHMENT_TABLE_NAME = "ATTACHMENTS";

  private Connection c = null;

  public HistoryService() {
    try {
      // Créé la connexion a la base de donnée
      c = DriverManager.getConnection(
          "jdbc:sqlite:" + System.getProperty("user.home") + "/.poo-chat.db");

      // Créé les tables nécessaires
      createTables();
    } catch (SQLException e) {
      System.err.println("Impossible d'ouvrir le fichier ~/.poo-chat.db");
      System.err.println(e.getMessage());
      e.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * Créé les tables de la base de donnée
   *
   * @throws SQLException en cas d'erreur à la création
   */
  private void createTables() throws SQLException {
    String messageTable = "create table if not exists " + MESSAGE_TABLE_NAME + " "
        + "(ID text PRIMARY KEY NOT NULL, "
        + "SOURCE_MAIL text, "
        + "DEST_MAIL dest, "
        + "TIMESTAMP timestamp, "
        + "TEXT text, "
        + "ATTACHMENT_ID text)";

    String attachmentTable = "create table if not exists " + ATTACHMENT_TABLE_NAME + " "
        + "(ID text PRIMARY KEY NOT NULL, "
        + "CONTENT blob)";

    try (Statement s = c.createStatement()) {
      s.executeUpdate(messageTable);
      s.executeUpdate(attachmentTable);
    } catch (SQLException e) {
      System.err.println("Impossible de créer les tables de la base de donnée");
      e.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * Ajoute un message à la base de donnée
   *
   * @param message le message à ajouter
   */
  public void addMessage(Message message) {
    String id = UUID.randomUUID().toString();
    String source_mail = message.getSource().getEmail();
    String dest_mail = message.getDest().getEmail();
    Timestamp timestamp = message.getTimestamp();
    String text = message.getText();
    String attachmentId = "";

    // Si il y a une pièce jointe, on l'ajoute à la base de donnée
    byte[] attachment = message.getAttachment();
    if (attachment != null) {
      attachmentId = UUID.randomUUID().toString();
      try {
        PreparedStatement ps =
            c.prepareStatement("insert into " + ATTACHMENT_TABLE_NAME + " values (?, ?)");
        ps.setString(1, attachmentId);
        ps.setBytes(2, attachment);

        ps.executeUpdate();
      } catch (SQLException sqle) {
        System.err.println("Sauvegarde de la pièce jointe impossible");
        sqle.printStackTrace();
      }
    }

    // On ajoute le message
    try {
      PreparedStatement ps =
          c.prepareStatement("insert into " + MESSAGE_TABLE_NAME + " values (?, ?, ?, ?, ?, ?)");
      ps.setString(1, id);
      ps.setString(2, source_mail);
      ps.setString(3, dest_mail);
      ps.setTimestamp(4, timestamp);
      ps.setString(5, text);
      ps.setString(6, attachmentId);

      ps.executeUpdate();
    } catch (SQLException e) {
      System.err.println("Sauvegarde du message impossible");
      e.printStackTrace();
    }
  }

  /**
   * Récupère l'ensemble de l'historique d'une conversation
   *
   * @param user1 un des utilisateurs de la conversation
   * @param user2 un des utilisateurs de la conversation
   * @return la liste triée des messages
   */
  public ArrayList<Message> getConversation(User user1, User user2) {
    // Requête qui récupère l'ensembles des messages de user1 à user2 et de user2 à user1
    // (elle inclue aussi les messages de user1 à user1 et user2 à user2, mais ça n'arrive jamais)
    String mail1 = user1.getEmail();
    String mail2 = user2.getEmail();
    String query = "select * from " + MESSAGE_TABLE_NAME + " where "
        + "(source_mail = \"" + mail1 + "\" or source_mail = \"" + mail2 + "\") "
        + "AND "
        + "(dest_mail = \"" + mail1 + "\" or dest_mail = \"" + mail2 + "\") "
        + "order by timestamp";

    // On récupère l'ensemble des résultats
    ArrayList<Message> messages = new ArrayList<Message>();
    try {
      Statement s = c.createStatement();
      ResultSet queryResults = s.executeQuery(query);
      while (queryResults.next()) {
        Timestamp timestamp = queryResults.getTimestamp("TIMESTAMP");
        String source_mail = queryResults.getString("SOURCE_MAIL");
        String dest_mail = queryResults.getString("DEST_MAIL");
        String text = queryResults.getString("TEXT");
        String attachmentId = queryResults.getString("ATTACHMENT_ID");

        // On récupère l'attachment si il existe
        byte[] attachment = null;
        if (attachmentId.length() != 0) {
          String attachmentQuery =
              "select * from " + ATTACHMENT_TABLE_NAME + " where id = \"" + attachmentId + "\"";
          ResultSet attachmentQueryResults = s.executeQuery(attachmentQuery);
          if (attachmentQueryResults.next()) {
            attachment = attachmentQueryResults.getBytes("CONTENT");
          } else {
            System.err.println("Pièce jointe manquante");
          }
        }

        // On forme le message
        Message m;
        if (user1.getEmail().equals(source_mail)) {
          m = new Message(timestamp, user1, user2, text, attachment);
        } else {
          m = new Message(timestamp, user2, user1, text, attachment);
        }
        messages.add(m);
      }
    } catch (SQLException sqle) {
      System.err.println("Erreur dans la récupération des données");
      sqle.printStackTrace();
    }

    return messages;
  }
}
