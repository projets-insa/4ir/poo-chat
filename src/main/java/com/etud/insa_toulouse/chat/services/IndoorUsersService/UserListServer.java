/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/UserListServer.java authors
 * : Clément Chaine (cc.chaine@free.fr) created     : Mar  5 jan 15:47:36 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.User;
import java.io.*;
import java.net.*;
import java.util.concurrent.Semaphore;

public class UserListServer {
  public final static int MAX_PACKET_SIZE = 5000;
  public final static int NOTIFY_PORT = 5201;
  public final static int WAIT_NOTIFICATION_PORT = 2054;

  private IndoorUsersService indoorUsersService;
  private User us;

  private Semaphore mutex;

  private WaitNotificationThread waitNotificationThread;

  /**
   * Constructeur
   *
   * @param indoorUsersService Un pointeur vers le communication service
   * @param us Un user représentant l'utilisateur
   */
  public UserListServer(IndoorUsersService indoorUsersService, User us) {
    this.indoorUsersService = indoorUsersService;
    this.us = us;
    this.mutex = new Semaphore(1);

    // La réponse aux notifications est dans le thread waitNotificationThread
    waitNotificationThread = new WaitNotificationThread(indoorUsersService, us, mutex);
    waitNotificationThread.start();
  }

  /**
   * Envoi un message en broadcast
   *
   * @param type Le type du message
   * @throws DisplayNameException En cas de displayName déjà prit
   */
  private void broadcast(UserListMessageType type) throws DisplayNameException {
    try {
      DatagramSocket socket = new DatagramSocket(NOTIFY_PORT);
      try {
        // Envoi du notify
        byte[] buffer = UserListServer.UserToByteArray(us, type);
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length,
            InetAddress.getByName("255.255.255.255"), UserListServer.WAIT_NOTIFICATION_PORT);
        socket.send(packet);

        // La boucle while sera interrompue si le temps entre deux réponses est
        // supérieur à 1000ms (par le SocketTimeoutException)
        socket.setSoTimeout(4000);
        buffer = new byte[UserListServer.MAX_PACKET_SIZE];
        packet = new DatagramPacket(buffer, buffer.length);
        while (true) {
          socket.receive(packet);
          UserListMessage ulm = UserListServer.ByteArrayToUserListMessage(buffer);
          // On renvoi une exception si le displayName est déjà utilisé
          if (ulm.getType() == UserListMessageType.DISPLAYNAME_ALREADY_TAKEN) {
            throw new DisplayNameException("DISPLAYNAME_ALREADY_TAKEN");
          }

          User user = ulm.getUser();
          if (user != null) {
            mutex.acquire();
            try {
              indoorUsersService.addUser(user);
            } catch (DisplayNameException e) {
              // On ne devrait arriver ici que quand type == CHANGE_DISPLAYNAME
              // La réponse entrainera forcément un DISPLAYNAME_ALREADY_EXISTS
            }
            mutex.release();
          }
        }
      } catch (SocketTimeoutException e) {
        socket.close();
      } catch (DisplayNameException e) {
        socket.close();
        throw e;
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (SocketException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Broadcast pour découvrir les utilisateurs connectés
   *
   * @throws DisplayNameException En cas de displayName déjà prit
   */
  public void discover() throws DisplayNameException {
    broadcast(UserListMessageType.NOTIFY);
  }

  /**
   * Broadcast pour change le displayName
   *
   * @throws DisplayNameException En cas de displayName déjà prit
   */
  public void updateUser() throws DisplayNameException {
    broadcast(UserListMessageType.CHANGE_DISPLAYNAME);
  }

  /** 
   * Broadcast pour déconnecter l'utilisateur 
   */
  public void disconnect() {
    try {
      // Cet appel devrait retourner vite car les autres utilisateurs ne doivent pas répondre
      broadcast(UserListMessageType.DISCONNECT);
    } catch (DisplayNameException e) {
      // This should never happen on a disconnect
      e.printStackTrace();
    }
  }

  /**
   * Met fin au thread d'attente de notification
   */
  public void close() {
    waitNotificationThread.kill();
  }

  /**
   * Converti un byte[] en UserListMessage
   *
   * @param buffer le byte[] à convertir
   */
  public static UserListMessage ByteArrayToUserListMessage(byte[] buffer) {
    UserListMessage m;
    try {
      ByteArrayInputStream byteStream = new ByteArrayInputStream(buffer);
      ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
      m = (UserListMessage) is.readObject();
      is.close();
    } catch (Exception e) {
      System.err.println("Impossible de convertir le byte array en User");
      System.err.println(e);
      m = null;
    }
    return m;
  }

  /**
   * Converti un User en byte[]
   *
   * @param user l'user à convertir
   * @param type le type de UserListMessage
   */
  public static byte[] UserToByteArray(User user, UserListMessageType type) {
    byte[] buffer = new byte[MAX_PACKET_SIZE];
    UserListMessage m = new UserListMessage(user, type);

    try {
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream(MAX_PACKET_SIZE);
      ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(byteStream));
      os.flush();
      os.writeObject(m);
      os.flush();
      buffer = byteStream.toByteArray();
      os.close();
    } catch (Exception e) {
      System.err.println("Impossible de convertir le User en byte array");
      System.err.println(e);
      buffer = null;
    }

    return buffer;
  }
}
