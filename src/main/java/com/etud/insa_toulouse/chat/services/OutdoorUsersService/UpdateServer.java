/*
 * File name   : /Users/cchaine/Library/Mobile
 * Documents/com~apple~CloudDocs/Etudes/INSA/4IR/poo/poo-chat/src/main/java/com/etud/insa_toulouse/chat/services/OutdoorUsersService/UpdateServer.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar 12 jan 10:04:23 2021
 *
 */

package com.etud.insa_toulouse.chat.services.OutdoorUsersService;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class UpdateServer extends Thread {
  private OutdoorUsersService outdoorUsersService;
  private HttpClient client;
  private HttpRequest request;

  private Consumer<Message> receiveHandler;

  private final AtomicBoolean running;
  private final AtomicBoolean stopped;

  /** 
   * Constructeur 
   * 
   * @param outdoorUsersService le parent
   * @param us l'utilisateur qui nous représente
   * @param receiveHandler callback lorsqu'un nouveau message est reçu
   */
  public UpdateServer(OutdoorUsersService outdoorUsersService, User us, Consumer<Message> receiveHandler) {
    this.outdoorUsersService = outdoorUsersService;
    this.receiveHandler = receiveHandler;
    this.client = HttpClient.newHttpClient();

    // Request URI with email as parameter
    URI uri = URI.create(OutdoorUsersService.SERVLET_URL + "?email=" + us.getEmail());
    this.request = HttpRequest.newBuilder().uri(uri).build();

    this.running = new AtomicBoolean(false);
    this.stopped = new AtomicBoolean(false);
  }

  /**
   * Appelée pour mettre fin à l'exécution de ce thread
   * Attend que le thread soit terminé pour retourner
   */
  public void kill() {
    running.set(false);
    while (!stopped.get()) {
    }
  }

  public void run() {
    running.set(true);

    while (running.get()) {
      HttpResponse<byte[]> response = null;
      try {
        response = client.send(request, HttpResponse.BodyHandlers.ofByteArray());

        // On vérifie que l'on a bien reçu
        if (response.statusCode() == 200) {
          UpdateMessage result = null;
          try {
            // On converti en UpdateMessage
            ByteArrayInputStream byteStream = new ByteArrayInputStream(response.body());
            ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(byteStream));
            result = (UpdateMessage) is.readObject();
            is.close();

            // On met a jour les informations
            outdoorUsersService.setConnectedUsers(result.getConnectedUsers());
            for (Message m : result.getNewMessages()) {
              receiveHandler.accept(m);
            }
          } catch (IOException e) {
            System.err.println("Impossible de convertir le byte array en UpdateMessage");
            e.printStackTrace();
          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }

          // On une seconde avant la prochaine update
          try {
            TimeUnit.SECONDS.sleep(1);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        } else {
          System.err.println(response.statusCode());
        }
      } catch (ConnectException e) {
        System.err.println("Le servlet est inaccessible");
        System.exit(1);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    stopped.set(true);
  }
}
