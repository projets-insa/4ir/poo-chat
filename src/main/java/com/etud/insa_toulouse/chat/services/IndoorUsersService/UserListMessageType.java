/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/UserListMessageType.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:47:31 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

/*
 * Les méthodes proposées ici permette de sérializer l'enum
 */
public enum UserListMessageType {
  UNKNOWN(0),
  NOTIFY(1),
  NOTIFY_RESPONSE(2),
  CHANGE_DISPLAYNAME(3),
  DISPLAYNAME_ALREADY_TAKEN(4),
  DISCONNECT(5);

  private final int value;

  /**
   * Constructeur
   *
   * @param value la valeur du type
   */
  UserListMessageType(int value) {
    this.value = value;
  }

  /**
   * Converti l'entier en UserListMessageType
   *
   * @param value l'entier à convertir
   * @return L'UserListMessageType après conversion
   */
  public static UserListMessageType fromValue(int value) {
    for (UserListMessageType type : values()) {
      if (type.value == value) {
        return type;
      }
    }

    return getDefault();
  }

  /**
   * Converti un UserListMessageType en entier
   *
   * @return L'entier converti
   */
  public int toValue() {
    return value;
  }

  /**
   * Renvoi une valeur par défaut, ici UNKNOWN
   *
   * @return renvoi la valeur de type par défaut
   */
  public static UserListMessageType getDefault() {
    return UNKNOWN;
  }
}
