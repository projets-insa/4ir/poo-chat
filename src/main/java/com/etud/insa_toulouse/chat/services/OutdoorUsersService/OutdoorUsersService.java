/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/OutdoorUsersService/OutdoorUsersService.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:54:12 2021
 *
 */

package com.etud.insa_toulouse.chat.services.OutdoorUsersService;

import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.services.CommunicationService;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Faute d'avoir réussi à faire fonctionner le websocket sur tomcat
 * nous allons utiliser un servlet Http avec une requette toutes les
 * 1s
 */

/*
 * Code status des réponses :
 *   - 200 : OK
 *   - 201 : DisplayName déjà pris
 *   - 404 : User non trouvé
 *   - 501 : Problème coté serveur
 */

public class OutdoorUsersService extends CommunicationService {
  public static final String SERVLET_URL =
      "https://srv-gei-tomcat.insa-toulouse.fr/PooChatBensialiChaine/chat";

  private UpdateServer updateServer;
  private Consumer<Message> receiveHandler;
  private BiConsumer<CommunicationService, User[]> usersChangedHandler;

  private HttpClient client;

  private User us;
  private HashMap<String, User> connectedUsers;

  /**
   * Constructeur
   *
   * @param usersChangedHandler callback lorsque la liste des utilisateur à changé
   * @param receiveHandler callback lorsqu'un nouveau message est reçu
   */
  public OutdoorUsersService(BiConsumer<CommunicationService, User[]> usersChangedHandler,
      Consumer<Message> receiveHandler) {
    this.receiveHandler = receiveHandler;
    this.usersChangedHandler = usersChangedHandler;

    client = HttpClient.newHttpClient();

    connectedUsers = new HashMap<>();
  }

  /**
   * Connecte l'utilisateur au service outdoor
   *
   * @param us l'utilisateur
   * @throws ServletUnreachableException en cas d'échec de connexion au servlet
   * @throws DisplayNameException en cas de nom d'utilisateur déjà pris
   * @throws IOException en cas de problème de sérialization de l'utilisateur
   */
  public void connect(User us)
      throws ServletUnreachableException, DisplayNameException, IOException {
    // On sérialise l'utilisateur
    try {
      byte[] buffer = User.toByteArray(us);
      if (buffer == null) {
        throw new IOException("Impossible de sérializer l'user");
      }

      // On envoi le message
      HttpRequest request = HttpRequest.newBuilder()
                                .uri(URI.create(OutdoorUsersService.SERVLET_URL + "/connect"))
                                .POST(HttpRequest.BodyPublishers.ofByteArray(buffer))
                                .build();
      HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
      if (response.statusCode() == 201) {
        throw new DisplayNameException("Le displayname est déjà pris");
      } else if (response.statusCode() != 200) {
        throw new ServletUnreachableException(
            "Impossible de se connecter : " + response.statusCode() + "\n" + response.body());
      }

      this.us = us;

      // On met en place une update toutes les secondes
      updateServer = new UpdateServer(this, us, receiveHandler);
      updateServer.start();
    } catch (InterruptedException | IOException e) {
      throw new ServletUnreachableException("Impossible de se connecter");
    }
  }

  /**
   * Déconnecte l'utilisateur du service outdoor
   */
  public void disconnect() {
    // L'appel à disconnect sur un service déjà déconnecté ne fait rien
    if (this.us != null) {
      try {
        byte[] buffer = User.toByteArray(us);
        if (buffer == null) {
          throw new IOException("Impossible de sérializer l'user");
        }

        // Fonction bloquante
        updateServer.kill();

        // On envoi le message
        HttpRequest request = HttpRequest.newBuilder()
                                  .uri(URI.create(OutdoorUsersService.SERVLET_URL + "/disconnect"))
                                  .POST(HttpRequest.BodyPublishers.ofByteArray(buffer))
                                  .build();
        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() != 200) {
          System.err.println("Impossible de se déconnecter");
          System.err.println(response.body());
        }
      } catch (InterruptedException | IOException e) {
        System.err.println("Erreur lors de la déconnection au servlet");
      }
    }
  }

  /**
   * Change le nom d'utilisateur
   *
   * @throws ServletUnreachableException en cas d'échec de connexion au servlet
   * @throws DisplayNameException en cas de nom d'utilisateur déjà pris
   */
  public void updateDisplayName() throws ServletUnreachableException, DisplayNameException {
    // On sérialise l'utilisateur
    try {
      byte[] buffer = User.toByteArray(this.us);
      if (buffer == null) {
        throw new IOException("Impossible de sérializer l'user");
      }

      // On envoi le message
      HttpRequest request = HttpRequest.newBuilder()
                                .uri(URI.create(OutdoorUsersService.SERVLET_URL + "/displayName"))
                                .POST(HttpRequest.BodyPublishers.ofByteArray(buffer))
                                .build();
      HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
      if (response.statusCode() == 201) {
        throw new DisplayNameException("Le displayname est déjà pris");
      } else if (response.statusCode() != 200) {
        throw new ServletUnreachableException(
            "Impossible de se connecter : " + response.statusCode() + "\n" + response.body());
      }
    } catch (InterruptedException | IOException e) {
      throw new ServletUnreachableException("Impossible de se connecter");
    }
  }

  /**
   * Envoi un message au servlet
   *
   * @param message le message à envoyer
   * @throws ServletUnreachableException en cas d'échec de connexion au servlet
   * @throws IOException en cas d'échec de la sérialization du message
   */
  public void send(Message message) throws ServletUnreachableException, IOException {
    try {
      byte[] buffer = Message.toByteArray(message);
      if (buffer == null) {
        throw new IOException("Impossible de sérializer le message");
      }

      // On envoi le message
      HttpRequest request = HttpRequest.newBuilder()
                                .uri(URI.create(OutdoorUsersService.SERVLET_URL + "/send"))
                                .POST(HttpRequest.BodyPublishers.ofByteArray(buffer))
                                .build();
      HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
      if (response.statusCode() != 200) {
        throw new ServletUnreachableException(
            "Impossible de se connecter : " + response.statusCode() + "\n" + response.body());
      }
    } catch (InterruptedException | IOException e) {
      throw new ServletUnreachableException("Impossible d'envoyer le message");
    }
  }

  /**
   * Appelé par l'UpdateServeur pour mettre à jour la liste des
   * utilisateurs connectés
   *
   * @param connectedUsers la nouvelle liste
   */
  public void setConnectedUsers(HashMap<String, User> connectedUsers) {
    this.connectedUsers = connectedUsers;
    this.usersChangedHandler.accept(this, connectedUsers.values().toArray(new User[0]));
  }
}
