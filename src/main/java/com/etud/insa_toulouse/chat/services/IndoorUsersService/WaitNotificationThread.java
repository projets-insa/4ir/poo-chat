/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/WaitNotificationThread.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:47:40 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

/**
 *  L'envoi du notify et la réponse aux notify sont dans
 *  des threads différents et sur deux ports différents.
 *  Cela permet à un utilisateur de répondre aux notify en
 *  même temps qu'il reçoit des réponses de notify.
 */
import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.User;
import java.io.IOException;
import java.net.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

public class WaitNotificationThread extends Thread {
  private IndoorUsersService indoorUsersService;
  private User us;
  private Semaphore mutex;
  private final AtomicBoolean running;

  private DatagramSocket socket;

  /**
   * Constructeur
   *
   * @param indoorUsersService Un pointeur vers le communication service
   * @param us Un user représentant l'utilisateur
   */
  public WaitNotificationThread(IndoorUsersService indoorUsersService, User us, Semaphore mutex) {
    this.indoorUsersService = indoorUsersService;
    this.us = us;
    this.mutex = mutex;
    this.running = new AtomicBoolean(false);

    try {
      socket = new DatagramSocket(UserListServer.WAIT_NOTIFICATION_PORT);
      socket.setSoTimeout(2000);
    } catch (SocketException e) {
      System.err.println(
          "Impossible de créer le socket sur le port " + UserListServer.WAIT_NOTIFICATION_PORT);
      System.err.println(e);
      System.exit(1);
    }
  }

  /**
   * Appelée pour mettre fin à l'exécution de ce thread
   */
  public void kill() {
    running.set(false);
  }

  /**
   * Envoi une réponse après la réception d'un notify
   */
  public void run() {
    running.set(true);

    byte[] buffer = new byte[UserListServer.MAX_PACKET_SIZE];
    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
    // Tant que le daemon doit tourner
    while (running.get()) {
      try {
        // On attend un packet
        // Il y a un timeout pour permettre l'arret du daemon
        // en cas d'appel à la fonction kill
        socket.receive(packet);
        UserListMessage m = UserListServer.ByteArrayToUserListMessage(buffer);
        User newUser = m.getUser();

        if (newUser != null) {
          // On vérifie qu'on ne répond pas à notre propre notify
          if (!newUser.getIp().equals(us.getIp())) {
            mutex.acquire();
            // On prend en charge l'ajout d'un utilisateur, le changement de displayName ou le
            // disconnect
            try {
              switch (m.getType()) {
                case NOTIFY:
                  indoorUsersService.addUser(newUser);
                  sendResponse(newUser, UserListMessageType.NOTIFY_RESPONSE);
                  break;
                case CHANGE_DISPLAYNAME:
                  indoorUsersService.changeRemoteUserDisplayName(newUser);
                  // Bien que la réponse soit inutile, cela permet de vérifier que le nom n'est pas
                  // déjà prit
                  sendResponse(newUser, UserListMessageType.NOTIFY_RESPONSE);
                  break;
                case DISCONNECT:
                  indoorUsersService.removeUser(newUser);
                  break;
              }
            } catch (DisplayNameException e) {
              sendResponse(newUser, UserListMessageType.DISPLAYNAME_ALREADY_TAKEN);
            }
            // Le release est hors du try catch pour éviter les deadlocks
            mutex.release();
          }
        }
      } catch (SocketTimeoutException ste) {
      } catch (Exception e) {
        System.err.println("Erreur: WaitNotificationThread - " + e.toString());
      }
    }

    // Ne pas oublier de fermer le socket en cas de kill
    socket.close();
  }

  /**
   * Envoi une réponse à l'utilisateur
   *
   * @param user le destinataire
   * @param type le type de réponse
   * @throws IOException en cas d'erreur de sérialization
   */
  private void sendResponse(User user, UserListMessageType type) throws IOException {
    byte[] buffer = UserListServer.UserToByteArray(us, type);
    DatagramPacket packet =
        new DatagramPacket(buffer, buffer.length, user.getIp(), UserListServer.NOTIFY_PORT);
    socket.send(packet);
  }
}
