/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/IndoorUsersService/ReceiveServer.java authors
 * : Clément Chaine (cc.chaine@free.fr) created     : Mar  5 jan 15:47:07 2021
 *
 */

package com.etud.insa_toulouse.chat.services.IndoorUsersService;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class ReceiveServer extends Thread {
  // Pas plus de 5 personnes peuvent envoyer des messages en même temps
  public static final int THREAD_POOL_SIZE = 5;

  private final Consumer<Message> receiveHandler;
  private final ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

  private AtomicBoolean running;
  private AtomicBoolean stopped;

  public ReceiveServer(Consumer<Message> receiveHandler) {
    this.receiveHandler = receiveHandler;
    this.running = new AtomicBoolean(false);
    this.stopped = new AtomicBoolean(false);
  }

  /**
   * Appelée pour mettre fin à l'exécution de ce thread
   * Attend que le thread soit terminé pour retourner
   */
  public void kill() {
    running.set(false);
    while (!stopped.get()) {
    }
  }

  public void run() {
    running.set(true);

    try {
      ServerSocket serverSocket = new ServerSocket(IndoorUsersService.RECEIVE_PORT);
      serverSocket.setSoTimeout(1000);
      while (running.get()) {
        try {
          Socket socket = serverSocket.accept();
          threadPool.submit(new ReceiveTask(socket, receiveHandler));
        } catch (SocketTimeoutException e) {
        }
      }
    } catch (IOException e) {
      // Arrête le programme car c'est une erreur critique
      System.err.println("Réception impossible");
      e.printStackTrace();
      System.exit(1);
    }

    stopped.set(true);
  }
}
