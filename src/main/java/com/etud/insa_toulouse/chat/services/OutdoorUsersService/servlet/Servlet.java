/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/OutdoorUsersService/servlet/Servlet.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Jeu 21 jan 15:08:29 2021
 *
 */

package com.etud.insa_toulouse.chat.services.OutdoorUsersService.servlet;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.services.OutdoorUsersService.UpdateMessage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * Code status des réponses :
 *   - 200 : OK
 *   - 201 : DisplayName déjà pris
 *   - 404 : User non trouvé
 *   - 501 : Problème coté serveur
 */

public class Servlet extends HttpServlet {
  private static HashMap<String, User> indoorUsers = new HashMap<>();
  private static HashMap<String, User> outdoorUsers = new HashMap<>();
  private static HashMap<String, ArrayList<Message>> messages = new HashMap<>();

  public Servlet() {}

  /**
   * Gère une requète GET
   *
   * @param req la requète
   * @param res la réponse
   */
  public void doGet(HttpServletRequest req, HttpServletResponse res) {
    // Récupère l'email de l'utilisateur depuis req
    String userEmail = req.getParameter("email");
    User user = null;
    if (indoorUsers.containsKey(userEmail)) {
      user = indoorUsers.get(userEmail);
    } else if (outdoorUsers.containsKey(userEmail)) {
      user = outdoorUsers.get(userEmail);
    }

    // Si on ne trouve pas l'utilisateur car il n'est pas connecté
    if (user != null) {
      // Forme la réponse
      UpdateMessage updateMessage = null;
      HashMap<String, User> usersToSend = new HashMap<>();
      if (user.isIndoor()) {
        // Si l'utilisateur est indoor, on lui transmet la liste des utilisateurs outdoor
        usersToSend.putAll(outdoorUsers);
        // On se supprime de la liste
        usersToSend.remove(user.getEmail());
        updateMessage = new UpdateMessage(usersToSend, messages.get(user.getEmail()));
      } else {
        // Si l'utilisateur est outdoor, on lui transmet l'ensemble des utilisateurs
        usersToSend.putAll(outdoorUsers);
        usersToSend.putAll(indoorUsers);
        // On se supprime de la liste
        usersToSend.remove(user.getEmail());
        updateMessage = new UpdateMessage(usersToSend, messages.get(user.getEmail()));
      }

      // On envoi la réponse
      try {
        ObjectOutputStream out = new ObjectOutputStream(res.getOutputStream());
        out.writeObject(updateMessage);
        out.flush();

        out.close();

        // Si la réponse s'est bien envoyé, on peut supprimer les messages du servlet
        messages.put(user.getEmail(), new ArrayList<>());
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else {
      try {
        res.setStatus(404);
        res.getWriter().println("Impossible de trouver l'utilisateur " + userEmail);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Gère une requète POST
   *
   * @param req
   * @param res
   */
  public void doPost(HttpServletRequest req, HttpServletResponse res) {
    String type = req.getPathInfo();
    switch (type) {
      case "/connect":
        handleConnect(req, res);
        break;
      case "/disconnect":
        handleDisconnect(req, res);
        break;
      case "/send":
        handleSend(req, res);
        break;
      case "/displayName":
        handleDisplayName(req, res);
        break;
      default:
        break;
    }
  }

  /**
   * Gère une connexion
   *
   * @param req
   * @param res
   */
  private void handleConnect(HttpServletRequest req, HttpServletResponse res) {
    try {
      ObjectInputStream ois = new ObjectInputStream(req.getInputStream());
      User user = (User) ois.readObject();

      if (checkDisplayName(indoorUsers, user.getDisplayName())
          || checkDisplayName(outdoorUsers, user.getDisplayName())) {
        // Si le nom est déjà présent
        res.setStatus(201);
      } else {
        // Sinon on ajoute l'utilisateur
        if (user.isIndoor()) {
          indoorUsers.put(user.getEmail(), user);
        } else {
          outdoorUsers.put(user.getEmail(), user);
        }

        // On créé une liste de messages pour lui
        messages.put(user.getEmail(), new ArrayList<>());

        res.setStatus(200);
      }
    } catch (Exception e) {
      e.printStackTrace();
      res.setStatus(501);
      try {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        res.getWriter().print(sw.toString());
      } catch (IOException ioException) {
        ioException.printStackTrace();
      }
    }
  }

  /**
   * Gère une déconnexion
   *
   * @param req
   * @param res
   */
  private void handleDisconnect(HttpServletRequest req, HttpServletResponse res) {
    try {
      ObjectInputStream ois = new ObjectInputStream(req.getInputStream());
      User user = (User) ois.readObject();

      if (user.isIndoor()) {
        indoorUsers.remove(user.getEmail());
      } else {
        outdoorUsers.remove(user.getEmail());
      }

      // On supprime sa liste de messages
      messages.remove(user.getEmail());

      res.setStatus(200);
    } catch (Exception e) {
      e.printStackTrace();
      res.setStatus(501);
      try {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        res.getWriter().print(sw.toString());
      } catch (IOException ioException) {
        ioException.printStackTrace();
      }
    }
  }

  /**
   * Vérifie si le displayName est déjà utilisé
   *
   * @param users la liste des utilisateurs
   * @param displayName le displayName
   * @return vrai si le nom est déjà présent
   */
  private boolean checkDisplayName(HashMap<String, User> users, String displayName) {
    // Vérifie les utilisateurs en local
    Iterator<User> it = users.values().iterator();
    boolean found = false;
    while (!found && it.hasNext()) {
      found = it.next().getDisplayName().equals(displayName);
    }
    return found;
  }

  /** 
   * Gère le changement de nom d'utilisateur
   * 
   * @param req 
   * @param res 
   */
  private void handleDisplayName(HttpServletRequest req, HttpServletResponse res) {
    try {
      ObjectInputStream ois = new ObjectInputStream(req.getInputStream());
      User user = (User) ois.readObject();
      String displayName = user.getDisplayName();

      // Vérifie dans les indoors et outdoors
      if (checkDisplayName(indoorUsers, displayName)
          || checkDisplayName(outdoorUsers, displayName)) {
        // Déjà pris
        res.setStatus(201);
      } else {
        // Change le displayName
        if (user.isIndoor()) {
          indoorUsers.get(user.getEmail()).setDisplayName(displayName);
        } else {
          outdoorUsers.get(user.getEmail()).setDisplayName(displayName);
        }
        res.setStatus(200);
      }
    } catch (Exception e) {
      e.printStackTrace();
      res.setStatus(501);
      try {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        res.getWriter().print(sw.toString());
      } catch (IOException ioException) {
        ioException.printStackTrace();
      }
    }
  }

  /**
   * Gère un envoi de message
   *
   * @param req
   * @param res
   */
  private void handleSend(HttpServletRequest req, HttpServletResponse res) {
    try {
      ObjectInputStream ois = new ObjectInputStream(req.getInputStream());
      Message message = (Message) ois.readObject();

      messages.get(message.getDest().getEmail()).add(message);

      res.setStatus(200);
    } catch (Exception e) {
      res.setStatus(501);
      try {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        res.getWriter().print(sw.toString());
      } catch (IOException ioException) {
        ioException.printStackTrace();
      }
    }
  }
}
