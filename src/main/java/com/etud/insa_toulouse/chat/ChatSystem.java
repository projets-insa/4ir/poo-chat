/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/ChatSystem.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:14:16 2021
 *
 */

package com.etud.insa_toulouse.chat;

import com.etud.insa_toulouse.chat.services.CommunicationService;
import com.etud.insa_toulouse.chat.services.HistoryService.HistoryException;
import com.etud.insa_toulouse.chat.services.HistoryService.HistoryService;
import com.etud.insa_toulouse.chat.services.IndoorUsersService.IndoorUsersService;
import com.etud.insa_toulouse.chat.services.OutdoorUsersService.OutdoorUsersService;
import com.etud.insa_toulouse.chat.services.OutdoorUsersService.ServletUnreachableException;
import com.etud.insa_toulouse.chat.views.Controller;
import com.etud.insa_toulouse.chat.views.View;
import com.etud.insa_toulouse.chat.views.Window;
import com.etud.insa_toulouse.chat.views.chat.ChatView;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;

public class ChatSystem {
  private User us;

  private Window window;

  private IndoorUsersService indoorUsersService;
  private OutdoorUsersService outdoorUsersService;
  private HistoryService historyService;

  // Permet de savoir si la machine peut accéder au Servlet
  private boolean isOutdoorEnabled = true;

  private HashMap<Controller, Consumer<Message>> onMessageCallbacks = new HashMap<>();
  private HashMap<Controller, Consumer<User[]>> onUserChangedCallbacks = new HashMap<>();

  private HashMap<String, User> connectedUsers = new HashMap<>();

  private Semaphore mutex = new Semaphore(1);

  /**
   * Constructeur
   */
  public ChatSystem() {
    // Création des services
    indoorUsersService = new IndoorUsersService(this::onUserChanged, this::onMessage);
    outdoorUsersService = new OutdoorUsersService(this::onUserChanged, this::onMessage);
    historyService = new HistoryService();

    // Création de la fenêtre
    window = new Window(this, this::onClose);
    window.createWindow();

    while (true) {
    }
  }

  /**
   * Connecte l'utilisateur au service indoor et outdoor
   *
   * @param us l'utilisateur nous représentant
   * @throws DisplayNameException en cas de nom d'utilisateur déjà prit
   * @throws ServletUnreachableException en cas d'échec de connexion au servlet
   */
  public void connect(User us) throws DisplayNameException, ServletUnreachableException {
    try {
      if (us.isIndoor()) {
        indoorUsersService.connect(us);
      }
      outdoorUsersService.connect(us);

      this.us = us;

      // On est connecté, on change de vue
      window.setCurrentView(new ChatView(this));
    } catch (DisplayNameException e) {
      disconnect();

      throw e;
    } catch (ServletUnreachableException e) {
      if (!us.isIndoor()) {
        // On est outdoor et le servlet est unreachable
        throw e;
      }

      // Si la machine n'est pas connectée à internet, on authorise quand même le local
      isOutdoorEnabled = false;

      this.us = us;

      // On est connecté, on change de vue
      window.setCurrentView(new ChatView(this));
    } catch (IOException e) {
      disconnect();
    }
  }

  /**
   * Déconnecte l'utilisateur des services indoor et outdoors
   */
  public void disconnect() {
    indoorUsersService.disconnect();
    outdoorUsersService.disconnect();
    this.us = null;
  }

  /**
   * Envoi un message texte
   *
   * @param message le message à envoyer
   * @throws IOException en cas de mauvaise envoi du message
   */
  public void send(Message message) throws IOException {
    if (message.getDest().isIndoor() && us.isIndoor()) {
      // On envoi en local si on est tous les deux en local
      indoorUsersService.send(message);
    } else {
      // Else impossible
      if (this.isOutdoorEnabled) {
        try {
          outdoorUsersService.send(message);
        } catch (ServletUnreachableException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Change le displayName de l'utilisateur
   *
   * @param displayName le nouveau displayname
   * @throws DisplayNameException en cas de displayName déjà pris
   */
  public void changeDisplayName(String displayName) throws DisplayNameException {
    boolean found = false;
    Iterator<User> it = connectedUsers.values().iterator();
    while (!found && it.hasNext()) {
      found = it.next().getDisplayName().equals(displayName);
    }

    if (found) {
      throw new DisplayNameException("DisplayName already taken");
    } else {
      this.us.setDisplayName(displayName);
      try {
        // On update en local que si on est indoor
        if (us.isIndoor()) {
          indoorUsersService.updateDisplayName();
        }

        // On update avec le servlet que si il est accessible
        if (isOutdoorEnabled) {
          outdoorUsersService.updateDisplayName();
        }
      } catch (ServletUnreachableException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Appelée lors que l'on ferme la fenêtre
   */
  private void onClose() {
    disconnect();
    System.exit(0);
  }

  /**
   * Appelée lorsque l'on reçoit un nouveau message
   *
   * @param message le nouveau message
   */
  private void onMessage(Message message) {
    try {
      // Cette fonction peut être appelée par deux services
      mutex.acquire();

      // Enregistre le message dans l'historique
      historyService.addMessage(message);

      // Incrémente le compteur des messages non lu
      // sans mélanger les instances d'utilisateur du message et de l'application
      User source = message.getSource();
      User localSourceInstance = connectedUsers.get(source.getEmail());
      if (localSourceInstance != null) {
        localSourceInstance.incrementUnreadMessages();

        // Appelle les callbacks pour update l'affichage
        for (Consumer<Message> callback : onMessageCallbacks.values()) {
          callback.accept(message);
        }
      } else {
        System.err.println("Unknown user");
        source.print();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    mutex.release();
  }

  /**
   * Permet aux controlleurs d'executer du code lors de l'event
   *
   * @param subscriber le controller qui a appelé
   * @param callback la callback à appeler en cas d'event
   */
  public void subcribeOnMessage(Controller subscriber, Consumer<Message> callback) {
    onMessageCallbacks.put(subscriber, callback);
  }

  /**
   * Appelée lorsque la liste des utilisateurs connectés change
   *
   * @param provider le service qui a fourni cette liste
   * @param users la nouvelle liste d'utilisateur
   */
  private void onUserChanged(CommunicationService provider, User[] users) {
    try {
      // Cette fonction peut être appelée par deux services
      mutex.acquire();

      // Met à jour connectedUsers en gardant les instances des users
      // pour ne pas perdre les informations locales
      HashMap<String, User> newConnectedUsers = new HashMap<>();

      // Sépare les utilisateurs indoor et outdoor
      HashMap<String, User> indoor = new HashMap<>();
      HashMap<String, User> outdoor = new HashMap<>();
      for (User user : connectedUsers.values()) {
        if (user.isIndoor()) {
          indoor.put(user.getEmail(), user);
        } else {
          outdoor.put(user.getEmail(), user);
        }
      }

      HashMap<String, User> buffer;
      if (provider instanceof IndoorUsersService) {
        // On ne touche pas aux utilisateurs de l'autre service
        newConnectedUsers.putAll(outdoor);
        buffer = indoor;
      } else {
        // On ne touche pas aux utilisateurs de l'autre service
        newConnectedUsers.putAll(indoor);
        buffer = outdoor;
      }

      // Permet de savoir si il y a eu du changement
      boolean changed = users.length != buffer.size();

      // On boucle sur la nouvelle liste :
      //  - Si l'utilisateur était déjà la avant, on ajoute l'instance précédente
      //  - Si l'instance précédente a été ajouté, on change le displayname si il a changé
      for (User user : users) {
        User buf = buffer.get(user.getEmail());
        if (buf != null) {
          if (!buf.getDisplayName().equals(user.getDisplayName())) {
            buf.setDisplayName(user.getDisplayName());
            changed = true;
          }
          newConnectedUsers.put(buf.getEmail(), buf);
        } else {
          newConnectedUsers.put(user.getEmail(), user);
          changed = true;
        }
      }

      if (changed) {
        connectedUsers = newConnectedUsers;

        // Appelle les callbacks pour update l'affichage
        for (Consumer<User[]> callback : onUserChangedCallbacks.values()) {
          callback.accept(connectedUsers.values().toArray(new User[0]));
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    mutex.release();
  }

  /**
   * Permet aux vues d'executer du code lors de l'event
   *
   * @param subscriber le controller qui a appelé
   * @param callback la callback à appeler en cas d'event
   */
  public void subscribeOnUserChanged(Controller subscriber, Consumer<User[]> callback) {
    onUserChangedCallbacks.put(subscriber, callback);
    // On appelle la callback pour que le subscriber ait les données initiales
    callback.accept(connectedUsers.values().toArray(new User[0]));
  }

  /**
   * Getter pour us
   *
   * @return us
   */
  public User getUs() {
    return us;
  }

  /**
   * Récupère une conversation
   *
   * @param user la deuxième personne
   * @return la conversation entre us et user
   */
  public Message[] getConversation(User user) {
    return historyService.getConversation(us, user).toArray(new Message[0]);
  }
}
