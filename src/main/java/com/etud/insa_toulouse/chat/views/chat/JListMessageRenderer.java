/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/chat/JListMessageRenderer.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:22 2021
 *
 */

package com.etud.insa_toulouse.chat.views.chat;

import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.views.JBadge;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/** 
 * Custom renderer for the Message list 
 */
public class JListMessageRenderer implements ListCellRenderer<Message> {
  User us;

  public JListMessageRenderer(User us) {
    this.us = us;
  }

  @Override
  public Component getListCellRendererComponent(JList<? extends Message> list, Message value, int index, boolean isSelected, boolean cellHasFocus) {
    JPanel cell = new JPanel(new BorderLayout());
    cell.setOpaque(false);

    Color messageColor;
    String messagePosition;
    if(value.getSource().getEmail().equals(us.getEmail())) {
      messageColor = Color.ORANGE;
      messagePosition = BorderLayout.LINE_END;
    } else {
      messageColor = Color.LIGHT_GRAY;
      messagePosition = BorderLayout.LINE_START;
    }

    JBadge message;
    if(value.getAttachment() == null) {
      message = new JBadge(value.getText(), messageColor);
      message.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
    } else {
      Icon attachIcon = new ImageIcon(getClass().getResource("/attach.png"));
      message = new JBadge(value.getText(), attachIcon, JLabel.LEFT, messageColor);
      message.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 14));
    }

    message.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    cell.add(message, messagePosition);

    return cell;
  }
}
