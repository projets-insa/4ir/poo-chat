/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/connexion/ConnexionController.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:42 2021
 *
 */

package com.etud.insa_toulouse.chat.views.connexion;

import com.etud.insa_toulouse.chat.ChatSystem;
import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.services.IndoorUsersService.IndoorUsersService;
import com.etud.insa_toulouse.chat.services.OutdoorUsersService.ServletUnreachableException;
import com.etud.insa_toulouse.chat.views.Controller;
import java.awt.*;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.swing.*;

public class ConnexionController extends Controller {
  private ConnexionView view;
  private ChatSystem chatSystem;

  private ArrayList<InetAddress> availableIps;

  /**
   * Constructeur
   *
   * @param view référence à la vue qui est controllee
   * @param chatSystem référence à la classe mère
   */
  public ConnexionController(ConnexionView view, ChatSystem chatSystem) {
    this.chatSystem = chatSystem;
    this.view = view;
  }

  /** 
   * Retourne la liste des interfaces réseau disponibles 
   * 
   * @return la liste des interfaces réseau disponible
   */
  public String[] getNetworkComboContent() {
    availableIps = new ArrayList<>();

    ArrayList<String> data = new ArrayList<>();
    data.add("Réseau Internet");

    Enumeration<NetworkInterface> netints = IndoorUsersService.getAllInterfaces();
    while (netints.hasMoreElements()) {
      NetworkInterface n = netints.nextElement();

      // On récupère la première adresse IPv4
      InetAddress ip = null;
      Enumeration<InetAddress> a = n.getInetAddresses();
      while (ip == null && a.hasMoreElements()) {
        ip = a.nextElement();
        if (ip instanceof Inet6Address) {
          ip = null;
        }
      }

      // On ajoute l'interface que si elle dispose d'une adresse IPv4
      if (ip != null) {
        availableIps.add(ip);
        data.add(n.getDisplayName() + " - " + ip.getHostAddress());
      }
    }

    return data.toArray(new String[0]);
  }

  /** 
   * Appelée lors de l'appuie sur le bouton connexion 
   */
  public void onConnexionButtonClick() {
    // On change le curseur pour qu'il soit en chargement
    view.getPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

    String email = view.getEmail();
    String displayName = view.getDisplayName();
    InetAddress ip = null;
    if (view.getNetwork() != 0) {
      ip = availableIps.get(view.getNetwork() - 1);
    }

    if (!email.isEmpty() && !displayName.isEmpty()) {
      // On appelle une callback de chatsystem
      User us = new User(email, ip, displayName);
      try {
        chatSystem.connect(us);

        // On est connecté
        // Pas d'action ici, c'est le chatsystem qui change la vue
      } catch (DisplayNameException e) {
        view.hideEmailError();
        view.showDisplayNameError();
        // On reset le cursor
        view.getPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        // On affiche un popup pour prévenir que le nom d'affichage est prit
        JOptionPane.showMessageDialog(new JFrame(), "Le nom d'utilisateur est déjà pris",
            "Connexion", JOptionPane.ERROR_MESSAGE);
      } catch (ServletUnreachableException e) {
        e.printStackTrace();
        view.hideEmailError();
        view.hideDisplayNameError();

        // On reset le cursor
        view.getPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        // On affiche un popup pour prévenir que le nom d'affichage est prit
        JOptionPane.showMessageDialog(new JFrame(), "Le serveur n'est pas disponible", "Connexion",
            JOptionPane.ERROR_MESSAGE);
      }

    } else {
      // Gère l'affichage des erreurs
      view.hideEmailError();
      view.hideDisplayNameError();

      if (email.isEmpty()) {
        view.showEmailError();
      }
      if (displayName.isEmpty()) {
        view.showDisplayNameError();
      }

      // On reset le cursor
      view.getPane().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }
  }
}
