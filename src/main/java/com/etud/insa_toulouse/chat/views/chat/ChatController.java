/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/chat/ChatController.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 15:38:47 2021
 *
 */

package com.etud.insa_toulouse.chat.views.chat;

import com.etud.insa_toulouse.chat.ChatSystem;
import com.etud.insa_toulouse.chat.DisplayNameException;
import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.views.Controller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import javax.swing.*;

public class ChatController extends Controller {
  private ChatView view;
  private ChatSystem chatSystem;

  private DefaultListModel userListModel;
  private DefaultListModel messageListModel;

  private User selectedUser = null;

  private File attachment = null;

  /**
   * Constructeur
   *
   * @param view référence à la vue qui est controllee
   * @param chatSystem référence à la classe mère
   */
  public ChatController(ChatView view, ChatSystem chatSystem) {
    this.view = view;
    this.chatSystem = chatSystem;

    userListModel = new DefaultListModel();
    messageListModel = new DefaultListModel();
  }

  /**
   * Appelée pour abonner le controlleur aux mises à jours
   */
  public void init() {
    // La vue doit être initialisé avant de subscribe
    chatSystem.subcribeOnMessage(this, this::updateOnMessage);
    chatSystem.subscribeOnUserChanged(this, this::updateOnUserChanged);
  }

  /**
   * Cette callback permet de mettre à jour la liste des utilisateurs
   *
   * @param users la nouvelle liste
   */
  private void updateOnUserChanged(User[] users) {
    // On met à jour le model
    userListModel.clear();
    User newSelectedUser = null;
    for (User user : users) {
      // On recherche l'utilisateur qui était sélectionné
      if (selectedUser != null) {
        if (user.getEmail().equals(selectedUser.getEmail())) {
          newSelectedUser = user;
        }
      }

      userListModel.addElement(user);
    }

    // On déselectionne si l'utilisateur est parti
    if (selectedUser != null && newSelectedUser == null) {
      attachment = null;
      view.getUserList().clearSelection();

      JOptionPane.showMessageDialog(
          view.getPane(), selectedUser.getDisplayName() + " s'est déconnecté");
    }
    selectedUser = newSelectedUser;
    // Si l'utilisateur est toujours là, on le resélectionne
    if (selectedUser != null) {
      view.getUserList().setSelectedValue(selectedUser, true);
    }

    // On met à jour la vue
    view.updateUserList();
    view.updateRightPane();
  }

  /**
   * Cette callback permet de mettre à jour la vue sans avoir à recharger l'historique entier
   *
   * @param message le nouveau message
   */
  private void updateOnMessage(Message message) {
    // On teste source et dest car on utilise cette fonction aussi lorsque l'on envoi
    if (selectedUser != null
        && (selectedUser.getEmail().equals(message.getSource().getEmail())
            || selectedUser.getEmail().equals(message.getDest().getEmail()))) {
      messageListModel.addElement(message);
      // Le message est read
      selectedUser.setUnreadMessages(0);
      // On update la vue
      view.updateRightPane();
    }

    // On update la vue du nombre de messages non lu
    view.updateUserList();
  }

  /**
   * Appelée lors de l'appuie sur le bouton send
   */
  public void onSendButtonClick() {
    Message message;
    if (attachment == null) {
      message = new Message(getUs(), selectedUser, view.getMessageTextField().getText(), null);
    } else {
      try {
        byte[] attachmentData = Files.readAllBytes(attachment.toPath());

        message = new Message(getUs(), selectedUser, attachment.getName(), attachmentData);
      } catch (IOException e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(view.getPane(), "Envoi de la pièce jointe impossible",
            "Pièce jointe", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }

    try {
      chatSystem.send(message);

      // On reset le champ de texte
      view.getMessageTextField().setText("");
      // On reset la pièce jointe
      attachment = null;

      // On ajoute le message à la vue
      updateOnMessage(message);
    } catch (IOException e) {
      e.printStackTrace();
      JOptionPane.showMessageDialog(
          view.getPane(), "Envoi du message impossible", "Message", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Appelée lors de l'appuie sur le bouton attach
   */
  public void onAttachButtonClick() {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    int result = fileChooser.showOpenDialog(view.getPane());
    if (result == JFileChooser.APPROVE_OPTION) {
      attachment = fileChooser.getSelectedFile();

      view.updateAttachmentBadge();
    }
  }

  /**
   * Appelée lors du click sur un message avec une pièce jointe
   *
   * @param message le message
   */
  public void onAttachDownloadClick(Message message) {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    fileChooser.setSelectedFile(new File(message.getText()));
    int result = fileChooser.showSaveDialog(view.getPane());
    if (result == JFileChooser.APPROVE_OPTION) {
      try {
        Files.write(fileChooser.getSelectedFile().toPath(), message.getAttachment(),
            StandardOpenOption.CREATE_NEW);
      } catch (IOException e) {
        // Si le fichier existe déjà, on affiche un popup
        JOptionPane.showMessageDialog(
            view.getPane(), "Le fichier existe déjà: " + fileChooser.getSelectedFile().toPath());
      }
    }
  }

  /**
   * Appelée lors de la suppression de la pièce jointe sélectionnée
   */
  public void onClearAttachButtonClick() {
    int confirm = JOptionPane.showConfirmDialog(view.getPane(),
        "Êtes-vous sûr de vouloir enlever la pièce jointe ?", "Pièce jointe",
        JOptionPane.YES_NO_OPTION);
    if (confirm == 0) {
      // OUI
      attachment = null;

      // Update l'affichage
      view.updateAttachmentBadge();
    }
  }

  /**
   * Appelée lors de l'appuie sur le bouton changer de displayname
   */
  public void onChangeDisplayNameButtonClick() {
    String newDisplayName = JOptionPane.showInputDialog(
        view.getPane(), "Veuillez entrer votre nouveau nom d'utilisateur :");
    try {
      chatSystem.changeDisplayName(newDisplayName);

      view.updateDisplayName();
      view.updateUserList();
    } catch (DisplayNameException e) {
      JOptionPane.showMessageDialog(view.getPane(), "Ce nom d'utilisateur est déjà utilisé",
          "Nom d'utilisateur", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Appelée lors de la sélection d'un utilisateur dans la liste des utilisateurs
   *
   * @param index l'indice de l'utilisateur sélectionné
   */
  public void onUserSelect(int index) {
    // Ne fait rien si c'est la déselection
    if (index >= 0) {
      selectedUser = (User) userListModel.get(index);

      // Récupère la conversation
      Message[] messages = chatSystem.getConversation(selectedUser);
      messageListModel.clear();
      for (Message message : messages) {
        messageListModel.addElement(message);
      }

      // Reset le nombre de messages non-lu de l'utilisateur
      selectedUser.setUnreadMessages(0);

      // Update l'affichage
      view.updateRightPane();
    } else {
      messageListModel.clear();
      view.updateRightPane();
    }
  }

  /**
   * Getter pour us
   *
   * @return us
   */
  public User getUs() {
    return chatSystem.getUs();
  }

  /**
   * Getter pour userListModel
   *
   * @return userListMost
   */
  public DefaultListModel getUserListModel() {
    return userListModel;
  }

  /**
   * Getter pour messageListModel
   *
   * @return messageListModel
   */
  public DefaultListModel getMessageListModel() {
    return messageListModel;
  }

  /**
   * Getter pour selectedUser
   *
   * @return selectedUser
   */
  public User getSelectedUser() {
    return selectedUser;
  }

  /**
   * Getter pour attachment
   *
   * @return attachment
   */
  public File getAttachment() {
    return attachment;
  }
}
