/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/JBadge.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:53 2021
 *
 */

package com.etud.insa_toulouse.chat.views;

import java.awt.*;
import javax.swing.*;

/**
 * Dessine du texte dans un cercle coloré avec optionnellement un icon
 */
public class JBadge extends JLabel {
  Color color;

  public JBadge(String text, Color color) {
    super(text);
    this.color = color;
  }

  public JBadge(String text, Icon icon, int horizontalAlignment, Color color) {
    super(text, icon, horizontalAlignment);
    this.color = color;
  }

  @Override
  protected void paintComponent(Graphics g) {
    // Enable antialiasing
    RenderingHints qualityHints =
        new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    ((Graphics2D) g).setRenderingHints(qualityHints);

    g.setColor(color);
    g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), this.getHeight(), this.getHeight());
    super.paintComponent(g);
  }
}
