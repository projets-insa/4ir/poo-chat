/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/JCircle.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:57 2021
 *
 */

package com.etud.insa_toulouse.chat.views;

import java.awt.*;
import javax.swing.*;

/**
 * Dessine un cercle coloré
 */
public class JCircle extends JPanel {
  Color color;
  boolean visible = true;

  public JCircle(Color color) {
    this.color = color;
  }

  @Override
  protected void paintComponent(Graphics g) {
    // Enable antialiasing
    RenderingHints qualityHints =
        new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    qualityHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    ((Graphics2D) g).setRenderingHints(qualityHints);

    if (visible) {
      g.setColor(color);
      g.fillOval(0, 0, g.getClipBounds().width, g.getClipBounds().height);
    }
  }

  public void setVisible(boolean value) {
    this.visible = value;
  }
}
