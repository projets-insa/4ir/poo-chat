/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/chat/ChatView.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:18 2021
 *
 */

package com.etud.insa_toulouse.chat.views.chat;

import static java.lang.Integer.max;

import com.etud.insa_toulouse.chat.ChatSystem;
import com.etud.insa_toulouse.chat.Message;
import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.views.JBadge;
import com.etud.insa_toulouse.chat.views.JCircle;
import com.etud.insa_toulouse.chat.views.View;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ChatView extends View {
  ChatController controller;

  private JScrollBar messageVerticalScrollBar;
  private JList userList;
  private JList messageList;
  private JLabel displayNameLabel;
  private JPanel attachmentBadgeContainer;
  private JScrollPane messageListScrollPane;
  private JButton sendButton;
  private JButton attachButton;
  private JTextField messageTextField;
  private JLabel destDisplayNameLabel;
  private JCircle destIsConnected;
  private JBadge attachmentBadge;

  public ChatView(ChatSystem chatSystem) {
    controller = new ChatController(this, chatSystem);
    createView();
    // The view needs to be created before calling init
    controller.init();
  }

  @Override
  public void createView() {
    GridBagConstraints c = new GridBagConstraints();
    JPanel view = new JPanel(new GridBagLayout());
    view.setBackground(Color.WHITE);

    /******************************************/
    /**************LEFT CONTAINER**************/
    /******************************************/
    JPanel leftContainer = new JPanel(new GridBagLayout());
    leftContainer.setBackground(Color.WHITE);

    /***************LEFT HEADER****************/
    JPanel leftHeaderContainer = new JPanel(new BorderLayout());
    leftHeaderContainer.setBackground(Color.WHITE);
    leftHeaderContainer.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));

    this.displayNameLabel = new JLabel(controller.getUs().getDisplayName());
    displayNameLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
    leftHeaderContainer.add(displayNameLabel, BorderLayout.LINE_START);

    Icon changeDisplayNameIcon = new ImageIcon(getClass().getResource("/edit.png"));
    JButton changeDisplayNameButton = new JButton(changeDisplayNameIcon);
    changeDisplayNameButton.setOpaque(false);
    changeDisplayNameButton.setPreferredSize(new Dimension(40, 40));
    changeDisplayNameButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.onChangeDisplayNameButtonClick();
      }
    });
    leftHeaderContainer.add(changeDisplayNameButton, BorderLayout.LINE_END);

    c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridheight = 1;
    c.weightx = 1;
    leftContainer.add(leftHeaderContainer, c);

    /***************LEFT CONTENT****************/
    JPanel leftContentContainer = new JPanel(new GridBagLayout());
    leftContentContainer.setBackground(Color.WHITE);

    // Create the list and put it in a scroll pane.
    this.userList = new JList(controller.getUserListModel());
    userList.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        // Seulement le premier index car le JList est en SINGLE_SELECTION
        if (!e.getValueIsAdjusting()) {
          controller.onUserSelect(userList.getSelectedIndex());
        }
      }
    });
    userList.setCellRenderer(new JListUserRenderer());
    userList.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    JScrollPane userListScrollPane = new JScrollPane(userList);
    userListScrollPane.setBorder(BorderFactory.createEmptyBorder());
    // Set the scrollbar speed
    JScrollBar verticalUserListScrollBar = userListScrollPane.getVerticalScrollBar();
    verticalUserListScrollBar.setUnitIncrement(5);

    c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.weighty = 1;
    leftContentContainer.add(userListScrollPane, c);

    c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 1;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.weighty = 1;
    leftContainer.add(leftContentContainer, c);

    /******************************************/
    /*************RIGHT CONTAINER**************/
    /******************************************/
    JPanel rightContainer = new JPanel(new GridBagLayout());
    rightContainer.setBackground(Color.WHITE);
    rightContainer.setBorder(BorderFactory.createMatteBorder(0, 1, 0, 0, Color.LIGHT_GRAY));

    /***************RIGHT HEADER****************/
    JPanel rightHeaderContainer = new JPanel(new BorderLayout());
    rightHeaderContainer.setBackground(Color.WHITE);
    rightHeaderContainer.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

    JPanel destInformationContainer = new JPanel();
    destInformationContainer.setOpaque(false);

    this.destDisplayNameLabel = new JLabel("");
    destDisplayNameLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
    destDisplayNameLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 18));
    destInformationContainer.add(destDisplayNameLabel);

    // Is connected circle
    this.destIsConnected = new JCircle(new Color(93, 212, 150));
    destIsConnected.setVisible(false);
    destInformationContainer.add(destIsConnected);

    rightHeaderContainer.add(destInformationContainer, BorderLayout.LINE_START);

    c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    c.gridheight = 1;
    c.fill = GridBagConstraints.HORIZONTAL;
    c.weightx = 1;
    rightContainer.add(rightHeaderContainer, c);

    /***************RIGHT CONTENT****************/
    JPanel rightContentContainer = new JPanel(new GridBagLayout());
    rightContentContainer.setBackground(Color.WHITE);

    // Create the list and put it in a scroll pane.
    this.messageList = new JList(controller.getMessageListModel());
    messageList.setCellRenderer(new JListMessageRenderer(controller.getUs()));
    messageList.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));
    messageList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    // Change le curseur en fonction de la cellule
    messageList.addMouseMotionListener(new MouseMotionListener() {
      public void mouseDragged(MouseEvent e) {
        updateCursor(e);
      }

      public void mouseMoved(MouseEvent e) {
        updateCursor(e);
      }

      private void updateCursor(MouseEvent e) {
        int messageIndex = messageList.locationToIndex(e.getPoint());
        final Rectangle messageCellBounds =
            messageList.getCellBounds(messageIndex, messageIndex + 1);
        if (messageCellBounds != null && messageCellBounds.contains(e.getPoint())) {
          Message message = (Message) messageList.getModel().getElementAt(messageIndex);
          if (message.getAttachment() != null) {
            messageList.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          } else {
            messageList.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          }
        } else {
          messageList.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
      }
    });
    messageList.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        // Vérifie si le message a une pièce jointe
        int messageIndex = messageList.locationToIndex(e.getPoint());
        final Rectangle messageCellBounds =
            messageList.getCellBounds(messageIndex, messageIndex + 1);
        if (messageCellBounds != null && messageCellBounds.contains(e.getPoint())) {
          Message message = (Message) messageList.getModel().getElementAt(messageIndex);
          if (message.getAttachment() != null) {
            controller.onAttachDownloadClick(message);
          }
        }
      }
    });

    this.messageListScrollPane = new JScrollPane(messageList);
    messageListScrollPane.setBorder(BorderFactory.createEmptyBorder());
    // Change la vitesse de défilement
    this.messageVerticalScrollBar = messageListScrollPane.getVerticalScrollBar();
    messageVerticalScrollBar.setUnitIncrement(5);

    c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.weighty = 1;
    rightContentContainer.add(messageListScrollPane, c);

    c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 1;
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1;
    c.weighty = 1;
    rightContainer.add(rightContentContainer, c);

    /***************RIGHT FOOTER****************/
    JPanel rightFooterContainer = new JPanel(new GridBagLayout());
    rightFooterContainer.setBackground(Color.WHITE);
    rightFooterContainer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    Icon attachIcon = new ImageIcon(getClass().getResource("/attach.png"));
    this.attachButton = new JButton(attachIcon);
    attachButton.setOpaque(false);
    attachButton.setPreferredSize(new Dimension(40, 40));
    attachButton.setEnabled(controller.getSelectedUser() != null);
    attachButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.onAttachButtonClick();
      }
    });
    rightFooterContainer.add(attachButton);

    this.attachmentBadgeContainer = new JPanel();
    attachmentBadgeContainer.setVisible(controller.getAttachment() != null);
    attachmentBadgeContainer.setOpaque(false);
    attachmentBadgeContainer.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    ImageIcon crossIcon = new ImageIcon(getClass().getResource("/cross.png"));
    this.attachmentBadge = new JBadge("", crossIcon, JLabel.RIGHT, Color.orange);
    attachmentBadge.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        controller.onClearAttachButtonClick();
      }
    });
    attachmentBadge.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    attachmentBadge.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
    attachmentBadge.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    attachmentBadgeContainer.add(attachmentBadge);
    rightFooterContainer.add(attachmentBadgeContainer);

    this.messageTextField = new JTextField();
    messageTextField.setBorder(BorderFactory.createCompoundBorder(
        messageTextField.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    messageTextField.setEnabled(controller.getSelectedUser() != null);
    messageTextField.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.onSendButtonClick();
      }
    });
    c = new GridBagConstraints();
    c.fill = GridBagConstraints.HORIZONTAL;
    c.weightx = 1;
    rightFooterContainer.add(messageTextField, c);

    Icon sendIcon = new ImageIcon(getClass().getResource("/send.png"));
    this.sendButton = new JButton(sendIcon);
    sendButton.setOpaque(false);
    sendButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.onSendButtonClick();
      }
    });
    sendButton.setEnabled(controller.getSelectedUser() != null);
    sendButton.setPreferredSize(new Dimension(40, 40));
    rightFooterContainer.add(sendButton);

    c = new GridBagConstraints();
    c.gridx = 0;
    c.anchor = GridBagConstraints.PAGE_END;
    c.fill = GridBagConstraints.BOTH;
    c.gridheight = 1;
    c.weightx = 1;
    c.weighty = 0;
    rightContainer.add(rightFooterContainer, c);

    // Ajoute les parties gauche et droites
    c = new GridBagConstraints();
    c.gridx = 0;
    c.gridy = 0;
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 1;
    c.weightx = 0.3;
    c.weighty = 1;
    view.add(leftContainer, c);

    c = new GridBagConstraints();
    c.gridx = 2;
    c.gridy = 0;
    c.fill = GridBagConstraints.BOTH;
    c.gridwidth = 2;
    c.weightx = 1;
    c.weighty = 1;
    view.add(rightContainer, c);

    pane = view;
  }

  /**
   * Permet de scroller en bas de la liste des messages
   */
  public void scrollMessagesToBottom() {
    // On s'assure que le pane est à jour
    messageListScrollPane.revalidate();
    // On l'invoke later pour que les éléments se soient bien ajoutés
    SwingUtilities.invokeLater(
        () -> { messageVerticalScrollBar.setValue(messageVerticalScrollBar.getMaximum()); });
  }

  /**
   * Met à jour le badge de l'attachment
   */
  public void updateAttachmentBadge() {
    File attachment = controller.getAttachment();
    if (attachment != null) {
      this.attachmentBadge.setText(attachment.getName());
    }
    this.messageTextField.setEnabled(attachment == null);
    this.attachmentBadgeContainer.setVisible(attachment != null);
    this.attachmentBadgeContainer.repaint();
  }

  /**
   * Met à jour le displayname
   */
  public void updateDisplayName() {
    this.displayNameLabel.setText(controller.getUs().getDisplayName());
  }

  /**
   * Met à jour la liste des utilisateurs
   */
  public void updateUserList() {
    userList.revalidate();
    userList.repaint();
  }

  /**
   * Met à jour tout le panel de droite
   */
  public void updateRightPane() {
    User selectedUser = controller.getSelectedUser();
    destDisplayNameLabel.setText(selectedUser == null ? "" : selectedUser.getDisplayName());
    destIsConnected.setVisible(selectedUser == null ? false : selectedUser.isConnected());
    destIsConnected.repaint();

    attachButton.setEnabled(selectedUser != null);
    messageTextField.setEnabled(selectedUser != null);
    sendButton.setEnabled(selectedUser != null);

    attachmentBadgeContainer.setVisible(controller.getAttachment() != null);

    messageList.revalidate();
    scrollMessagesToBottom();
  }

  /**
   * Getter pour userList
   *
   * @return userList
   */
  public JList getUserList() {
    return userList;
  }

  /**
   * Getter pour messageTextField
   *
   * @return messageTextField
   */
  public JTextField getMessageTextField() {
    return messageTextField;
  }
}
