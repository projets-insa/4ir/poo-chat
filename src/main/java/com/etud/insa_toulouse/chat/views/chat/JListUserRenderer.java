/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/chat/JListUserRenderer.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:27 2021
 *
 */

package com.etud.insa_toulouse.chat.views.chat;

import com.etud.insa_toulouse.chat.User;
import com.etud.insa_toulouse.chat.views.JBadge;
import com.etud.insa_toulouse.chat.views.JCircle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/** 
 * Custom renderer for the user list 
 */
public class JListUserRenderer implements ListCellRenderer<User> {

  @Override
  public Component getListCellRendererComponent(JList<? extends User> list, User value, int index, boolean isSelected, boolean cellHasFocus) {
    JPanel cell = new JPanel(new BorderLayout());
    if(isSelected) {
      cell.setBackground(Color.orange);
    } else {
      cell.setBackground(Color.WHITE);
    }
    cell.setBorder(BorderFactory.createEmptyBorder(10, 5, 5, 5));

    JLabel displayName = new JLabel(value.getDisplayName());
    displayName.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    if(value.getUnreadMessages() != 0) {
      displayName.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 13));
    }
    cell.add(displayName, BorderLayout.LINE_START);

    JPanel rightSide = new JPanel();
    rightSide.setOpaque(false);

    if(value.getUnreadMessages() != 0) {
      JPanel badgeContainer = new JPanel(new GridBagLayout());
      badgeContainer.setOpaque(false);
      badgeContainer.setBorder(BorderFactory.createEmptyBorder(0, 0 , 0, 5));
      JBadge unreadBadge = new JBadge("" + value.getUnreadMessages(), new Color(222, 62, 53));
      unreadBadge.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
      unreadBadge.setBorder(BorderFactory.createEmptyBorder(3, 5, 2, 5));
      unreadBadge.setForeground(Color.WHITE);
      badgeContainer.add(unreadBadge);
      rightSide.add(badgeContainer);
    }

    if(value.isConnected()) {
      JCircle isConnected = new JCircle(new Color(93, 212, 150));
      rightSide.add(isConnected);
    }

    cell.add(rightSide, BorderLayout.LINE_END);

    return cell;
  }
}
