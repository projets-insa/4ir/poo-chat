/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/View.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:14:06 2021
 *
 */

package com.etud.insa_toulouse.chat.views;

import javax.swing.*;

public abstract class View {
  protected JPanel pane;

  public abstract void createView();

  public JPanel getPane() {
    return pane;
  }
}
