/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/JImagePanel.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:14:01 2021
 *
 */

package com.etud.insa_toulouse.chat.views;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * JPanel avec une image en arrière plan
 */
public class JImagePanel extends JPanel {
  private BufferedImage image;

  public JImagePanel(BufferedImage background) {
    super();

    image = background;
  }

  public JImagePanel(LayoutManager layout, BufferedImage background) {
    super(layout);

    image = background;
  }

  @Override
  protected void paintComponent(Graphics g) {
    // super.paintComponents(g);
    if (getWidth() > getHeight()) {
      g.drawImage(image.getScaledInstance(getWidth(), -1, Image.SCALE_SMOOTH), 0, 0, this);
    } else {
      g.drawImage(image.getScaledInstance(-1, getHeight(), Image.SCALE_SMOOTH), 0, 0, this);
    }
  }
}
