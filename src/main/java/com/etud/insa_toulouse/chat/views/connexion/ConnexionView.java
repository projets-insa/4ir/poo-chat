/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/connexion/ConnexionView.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:13:45 2021
 *
 */

package com.etud.insa_toulouse.chat.views.connexion;

import com.etud.insa_toulouse.chat.ChatSystem;
import com.etud.insa_toulouse.chat.views.JCircle;
import com.etud.insa_toulouse.chat.views.JImagePanel;
import com.etud.insa_toulouse.chat.views.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.*;

public class ConnexionView extends View {
  ConnexionController controller;

  JTextField emailTextField;
  JCircle emailError;
  JTextField displayNameTextField;
  JCircle displayNameError;
  JComboBox<String> networkComboBox;

  private BufferedImage backgroundImage;
  private Font futura;

  public ConnexionView(ChatSystem chatSystem) {
    controller = new ConnexionController(this, chatSystem);

    try {
      backgroundImage = ImageIO.read(getClass().getResource("/background.png"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      InputStream is = getClass().getResourceAsStream("/futura.ttf");
      futura = Font.createFont(Font.TRUETYPE_FONT, is);
    } catch (FontFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    createView();
  }

  @Override
  public void createView() {
    JImagePanel view = new JImagePanel(new GridBagLayout(), backgroundImage);
    JPanel container = new JPanel();
    container.setOpaque(false);
    container.setLayout(new BoxLayout(container, BoxLayout.PAGE_AXIS));
    container.setBorder(BorderFactory.createEmptyBorder(0, 0, 50, 0));

    JPanel titleContainer = new JPanel();
    titleContainer.setOpaque(false);
    titleContainer.setBorder(BorderFactory.createEmptyBorder(0, 0, 30, 0));
    JLabel title = new JLabel("Chat System");
    Font sizedFutura = futura.deriveFont(50f);
    title.setFont(sizedFutura);
    titleContainer.add(title);
    container.add(titleContainer);

    JPanel emailContainer = new JPanel();
    emailContainer.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
    emailContainer.setOpaque(false);
    JLabel emailLabel = new JLabel("Email:");
    this.emailTextField = new JTextField(20);
    emailTextField.setBorder(BorderFactory.createCompoundBorder(
        emailTextField.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    this.emailError = new JCircle(new Color(222, 62, 53));
    emailError.setVisible(false);
    emailError.setBackground(Color.WHITE);
    emailContainer.add(emailLabel);
    emailContainer.add(emailTextField);
    emailContainer.add(emailError);
    container.add(emailContainer);

    JPanel displayNameContainer = new JPanel();
    displayNameContainer.setOpaque(false);
    JLabel displayNameLabel = new JLabel("Surnom:");
    this.displayNameTextField = new JTextField(20);
    displayNameTextField.setBorder(BorderFactory.createCompoundBorder(
        displayNameTextField.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    this.displayNameError = new JCircle(new Color(222, 62, 53));
    displayNameError.setVisible(false);
    displayNameError.setBackground(Color.WHITE);
    displayNameContainer.add(displayNameLabel);
    displayNameContainer.add(displayNameTextField);
    displayNameContainer.add(displayNameError);
    container.add(displayNameContainer);

    JPanel networkContainer = new JPanel();
    networkContainer.setOpaque(false);
    JLabel networkLabel = new JLabel("Réseau:");
    this.networkComboBox = new JComboBox(controller.getNetworkComboContent());
    networkComboBox.setBorder(BorderFactory.createCompoundBorder(
        networkComboBox.getBorder(), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    networkContainer.add(networkLabel);
    networkContainer.add(networkComboBox);
    container.add(networkContainer);

    JPanel connectButtonContainer = new JPanel();
    connectButtonContainer.setOpaque(false);
    connectButtonContainer.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    JButton connectButton = new JButton("Connexion");
    connectButton.setMargin(new Insets(7, 13, 7, 13));
    connectButton.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
    connectButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        controller.onConnexionButtonClick();
      }
    });
    connectButtonContainer.add(connectButton);
    container.add(connectButtonContainer);

    view.add(container);

    pane = view;
  }

  /** 
   * Affiche un temoin d'erreur pour le mail 
   */
  public void showEmailError() {
    emailError.setVisible(true);
    emailError.repaint();
  }

  /** 
   * Cache un temoin d'erreur pour le mail 
   */
  public void hideEmailError() {
    emailError.setVisible(false);
    emailError.repaint();
  }

  /** 
   * Affiche un temoin d'erreur pour le displayName 
   */
  public void showDisplayNameError() {
    displayNameError.setVisible(true);
    displayNameError.repaint();
  }

  /** 
   * Cache un temoin d'erreur pour le displayName 
   */
  public void hideDisplayNameError() {
    displayNameError.setVisible(false);
    displayNameError.repaint();
  }

  /** 
   * Getter pour displayName 
   * 
   * @return dispayName
   */
  public String getDisplayName() {
    return this.displayNameTextField.getText();
  }

  /** 
   * Getter pour email 
   * 
   * @return email
   */
  public String getEmail() {
    return this.emailTextField.getText();
  }

  /** 
   * Getter pour network 
   * 
   * @return network
   */
  public int getNetwork() {
    return networkComboBox.getSelectedIndex();
  }

}
