/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/views/Window.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:14:09 2021
 *
 */

package com.etud.insa_toulouse.chat.views;

import com.etud.insa_toulouse.chat.ChatSystem;
import com.etud.insa_toulouse.chat.views.chat.ChatView;
import com.etud.insa_toulouse.chat.views.connexion.ConnexionView;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

public class Window {
  private static final String windowName = "Chat";
  private static final Dimension defaultWindowDimension = new Dimension(800, 800);

  private static String OS = System.getProperty("os.name").toLowerCase();
  private static boolean IS_UNIX =
      (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);

  private JFrame frame;
  private View currentView;

  private ChatSystem chatSystem;
  private Runnable onClose;

  /**
   * Constructeur
   *
   * @param chatSystem parent
   * @param onClose callback lors de la fermeture de la fenêtre
   */
  public Window(ChatSystem chatSystem, Runnable onClose) {
    this.chatSystem = chatSystem;
    this.onClose = onClose;
  }

  /**
   * Créé la fenêtre
   */
  public void createWindow() {
    try {
      if (IS_UNIX) {
        // On veut forcer GTK sur linux
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
      } else {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      }
    } catch (Exception e) {
    }

    // Active l'accélération hardware
    System.setProperty("sun.java2d.opengl", "true");

    frame = new JFrame(windowName);
    frame.setPreferredSize(defaultWindowDimension);
    frame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        onClose.run();
      }
    });

    setCurrentView(new ConnexionView(chatSystem), true);
  }

  /**
   * Change la vue à afficher
   *
   * @param view la nouvelle vue
   */
  public void setCurrentView(View view) {
    setCurrentView(view, false);
  }

  /**
   * Change la vue à afficher et spécifie si la fenêtre doit être recentrée
   *
   * @param view la nouvelle vue
   * @param recenter si la fenêtre doit être recentrée
   */
  public void setCurrentView(View view, boolean recenter) {
    currentView = view;
    frame.setContentPane(currentView.getPane());
    frame.pack();
    if (recenter) {
      frame.setLocationRelativeTo(null);
    }
    // Disable autofocus
    frame.getContentPane().requestFocusInWindow();
    frame.setVisible(true);
  }
}
