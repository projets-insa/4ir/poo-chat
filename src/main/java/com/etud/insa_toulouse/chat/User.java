/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/services/CommunicationService/User.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Jeu 17 déc 17:26:28 2020
 *
 */

package com.etud.insa_toulouse.chat;

import java.io.*;
import java.net.InetAddress;

public class User implements Serializable {
  private String email;
  private InetAddress ip;
  private String displayName;

  // Ces informations sont locales
  private boolean isConnected;
  private int unreadMessages;

  /**
   * Constructeur utilisé pour l'interface graphique
   *
   * @param email mail de l'utilisateur
   * @param ip l'adresse ip associé à l'utilisateur
   * @param displayName le nom d'affichage de l'utilisateur
   * @param isConnected si l'utilisateur est actuellement connecté
   */
  public User(
      String email, InetAddress ip, String displayName, boolean isConnected, int unreadMessages) {
    this.email = email;
    this.ip = ip;
    this.displayName = displayName;
    this.isConnected = isConnected;
    this.unreadMessages = unreadMessages;
  }

  /**
   * Constructeur
   * @param email mail de l'utilisateur
   * @param ip l'address ip associé à l'utilisateur
   * @param displayName le nom d'affichage de l'utilisateur
   */
  public User(String email, InetAddress ip, String displayName) {
    this(email, ip, displayName, true, 0);
  }

  /**
   * Constructeur pour un outdoor user
   *
   * @param email mail de l'utilisateur
   * @param displayName le nom d'affichage de l'utilisateur
   */
  public User(String email, String displayName) {
    this(email, null, displayName, true, 0);
  }

  /**
   * Vérifie si l'utilisateur est indoor
   *
   * @return si l'utilisateur est indoor
   */
  public boolean isIndoor() {
    return this.ip != null;
  }

  /**
   * Getter pour l'email
   *
   * @return email
   */
  public String getEmail() {
    return email;
  }

  /**
   * Getter pour l'adresse ip
   *
   * @return ip
   */
  public InetAddress getIp() {
    return ip;
  }

  /**
   * Getter pour le nom d'affichage
   *
   * @return displayName
   */
  public String getDisplayName() {
    return displayName;
  }

  /**
   * Setter pour le nom d'affichage
   *
   * @param displayName le nouveau displayName
   */
  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  /**
   * Getter pour isConnected
   *
   * @return isConnected
   */
  public boolean isConnected() {
    return isConnected;
  }

  /**
   * Setter pour unreadMessage
   *
   * @param unreadMessages
   */
  public void setUnreadMessages(int unreadMessages) {
    this.unreadMessages = unreadMessages;
  }

  public void incrementUnreadMessages() {
    this.unreadMessages++;
  }

  /**
   * Getter pour unreadMessage
   *
   * @return unreadMessage
   */
  public int getUnreadMessages() {
    return unreadMessages;
  }

  /**
   * Affiche l'utilisateur
   */
  public void print() {
    if (isIndoor()) {
      System.out.print("Indoor ");
    } else {
      System.out.print("Outdoor ");
    }
    System.out.println("user");

    System.out.println("Email: " + email);

    System.out.println("DisplayName: " + displayName);

    if (isIndoor()) {
      System.out.println("IP: " + ip.getHostAddress());
    }

    System.out.println("");
  }

  /**
   * Convertie l'user en byte[]
   *
   * @param user l'utilisateur à convertir
   * @return le byte[]
   */
  public static byte[] toByteArray(User user) {
    byte[] buffer = null;
    try {
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(byteStream));
      os.flush();
      os.writeObject(user);
      os.flush();
      buffer = byteStream.toByteArray();
      os.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return buffer;
  }
}
