/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/App.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  9 fév 16:14:12 2021
 *
 */

package com.etud.insa_toulouse.chat;

public class App {
  public static void main(String[] argv) {
    // Active l'antialiasing pour swing
    System.setProperty("awt.useSystemAAFontSettings", "on");
    System.setProperty("swing.aatext", "true");

    new ChatSystem();
  }
}
