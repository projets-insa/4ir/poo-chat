/*
 * File name   : src/main/java/com/etud/insa_toulouse/chat/DisplayNameException.java
 * authors     : Clément Chaine (cc.chaine@free.fr)
 * created     : Mar  5 jan 15:46:49 2021
 *
 */

package com.etud.insa_toulouse.chat;

public class DisplayNameException extends Exception {
  /**
   * Constructeur
   *
   * @param message Le message associé à l'exception
   */
  public DisplayNameException(String message) {
    super(message);
  }
}
