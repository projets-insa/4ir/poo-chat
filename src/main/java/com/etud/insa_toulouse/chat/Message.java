/*
 * File name   :
 * src/main/java/com/etud/insa_toulouse/chat/services/CommunicationService/Message.java authors :
 * Clément Chaine (cc.chaine@free.fr) created     : Dim 20 déc 12:36:29 2020
 *
 */

package com.etud.insa_toulouse.chat;

import java.io.*;
import java.sql.Timestamp;

public class Message implements Serializable {
  private final Timestamp timestamp;

  private final User source;
  private final User dest;
  private final String text;
  private final byte[] attachment;

  /**
   * Constructeur utilisé lors de la récupération de l'historique
   *
   * @param timestamp  le timestamp du message
   * @param source     l'user source
   * @param dest       l'user dest
   * @param text       le text (ou le nom du fichier si il y a une pièce jointe)
   * @param attachment la pièce jointe
   */
  public Message(Timestamp timestamp, User source, User dest, String text, byte[] attachment) {
    this.timestamp = timestamp;
    this.source = source;
    this.dest = dest;
    this.text = text;
    this.attachment = attachment;
  }

  /**
   * Constructeur utilisé lors de l'envoi d'une pièce jointe
   *
   * @param source     l'user source
   * @param dest       l'user dest
   * @param text       le nom de la pièce jointe
   * @param attachment la pièce jointe
   */
  public Message(User source, User dest, String text, byte[] attachment) {
    this(new Timestamp(System.currentTimeMillis()), source, dest, text, attachment);
  }

  /**
   * Constructeur utilisé lors de l'envoi d'un message texte
   *
   * @param source l'user source
   * @param dest l'user dest
   * @param text le text
   */
  public Message(User source, User dest, String text) {
    this(new Timestamp(System.currentTimeMillis()), source, dest, text, null);
  }

  /**
   * Getter for the timestamp
   *
   * @return le timestamp
   */
  public Timestamp getTimestamp() {
    return this.timestamp;
  }

  /**
   * Getter for the source
   *
   * @return the source
   */
  public User getSource() {
    return this.source;
  }

  /**
   * Getter for the dest
   *
   * @return the dest
   */
  public User getDest() {
    return this.dest;
  }

  /**
   * Getter for the text
   *
   * @return le texte ou le nom du fichier attaché
   */
  public String getText() {
    return this.text;
  }

  /**
   * Getter for the attachement
   *
   * @return le contenu du fichier attaché
   */
  public byte[] getAttachment() {
    return this.attachment;
  }

  /**
   * Affiche le message dans la console
   */
  public void print() {
    System.out.println(timestamp.toString());

    System.out.print("Message de " + source.getDisplayName());
    if (source.isIndoor()) {
      System.out.print(" (" + source.getIp().getHostAddress() + ")");
    } else {
      System.out.print(" (" + source.getEmail() + ")");
    }
    System.out.print(" à " + dest.getDisplayName());
    if (dest.isIndoor()) {
      System.out.print(" (" + dest.getIp().getHostAddress() + ")");
    } else {
      System.out.print(" (" + dest.getEmail() + ")");
    }
    System.out.print("\n");

    System.out.println(text);

    if (attachment != null) {
      System.out.println("Pièce jointe de " + attachment.length + " octets");
    } else {
      System.out.println("Pas de pièce jointe");
    }
    System.out.println("");
  }

  /**
   * Converti un message en byte[]
   *
   * @param message le message à convertir
   * @return le byte[]
   */
  public static byte[] toByteArray(Message message) {
    byte[] buffer = null;
    try {
      ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
      ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(byteStream));
      os.flush();
      os.writeObject(message);
      os.flush();
      buffer = byteStream.toByteArray();
      os.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return buffer;
  }
}
