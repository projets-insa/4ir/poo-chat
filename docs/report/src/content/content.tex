\maketitle

\begin{abstract}
\textbf{Chat System} is a project carried out during the course of the 7th semester, in the Object-Oriented Programming course. This is a cross-platform real-time messaging application working on local networks as well as through the internet.

\vspace{1em}
The development was divided into three phases. The different services for listing users, sending messages and keeping a history of the sent data were implemented. The User Interface was then created before integrating everything into a complete system.

\vspace{1em}
The \texttt{UML} specification is in appendix as well as on the following \texttt{git} repository, alongside the entire codebase: \url{https://gitlab.com/projets-insa/4ir/poo-chat}
\end{abstract}

\newpage
\tableofcontents

\newpage

\pagenumbering{arabic}

\section{Installation}

Our project is base upon the \texttt{Maven} build system to easily manage our dependencies. The following command will compile the project and run the application.

\vspace{1em}
\texttt{mvn clean compile exec:java}
\vspace{1em}

Compiling the servlet to a \texttt{.war} file can be done through the following command. The generated file is located in the \texttt{target/} directory.

\vspace{1em}
\texttt{mvn clean compile war:war}
\vspace{1em}

\texttt{Tomcat} can be subject to upload size limitations. Reducing the size of the generated \texttt{war} file can be achieved by changing the scope of the \texttt{sqlite} dependency to \texttt{provided} in the \texttt{pom.xml} file.

\section{User guide}
\subsection{Logging in}

The user has to input three values to be able to log in the chat system :
\begin{itemize}
    \item[-] The \texttt{email} is the unique identifier for the user in the system
    \item[-] The \texttt{displayName} is a casual name which doesn't have to be unique. Though it cannot be used by multiple users connected at the same time.
    \item[-] The \texttt{network} is the local network on which the user wants to chat on. The \texttt{Réseau internet} option should be selected if the user is not on the internal company network.
\end{itemize}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node {\includegraphics[scale=0.4]{images/connection.png}};
    \end{tikzpicture}
    \caption{Connection view}
    \label{fig:connection}
\end{figure}

In case the \texttt{displayName} is already taken, a popup will appear to let the user know, and he will be able to try another one.

Another popup can appear if the internet is the selected network and the servlet is unreachable.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (-3, 0) node{\includegraphics[scale=0.4]{images/already_taken.png}};
        \draw (3, 0) node {\includegraphics[scale=0.4]{images/unreachable.png}};
    \end{tikzpicture}
    \caption{Connection popups}
    \label{fig:connection_popups}
\end{figure}

\subsection{Changing the displayName}

The \texttt{displayName} can be changed by clicking the edit button on the top right corner of the left panel (see figure \ref{fig:change_displayname_button}). A popup will then appear asking the user to input the new \texttt{displayName}. If it is already taken by another connected user, a new popup will inform the user.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node {\includegraphics[scale=0.4]{images/user_list.png}};
        \draw (1, 0) node[below] {\footnotesize{\texttt{Button to edit the displayName}}};
        \draw[->] (1, 0.1) -- (-1.9, 3.4);
    \end{tikzpicture}
    \caption{Change displayName button}
    \label{fig:change_displayname_button}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/change_displayname.png}};
    \end{tikzpicture}
    \caption{Change displayName dialog}
    \label{fig:change_displayname}
\end{figure}

\subsection{Sending a message}

To send a message, the user needs to select a conversation by clicking on a \texttt{displayName} in the left user list. The text can be entered in the text field on the bottom of the right panel. The message can then be sent by clicking the \texttt{send} button or pressing the \texttt{enter} key.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/conversation.png}};
        \draw [->] (0, 0) -- (1, -3.5);
        \draw (0, 0) node[above] {\footnotesize{\texttt{Message field}}};
        \draw [->] (3.5, -1) -- (4, -3.5);
        \draw (3, -1) node[above] {\footnotesize{\texttt{Send button}}};
    \end{tikzpicture}
    \caption{Conversation view}
    \label{fig:conversation_view}
\end{figure}

The message will either appear in the conversation view of the recipient if it is selected, or the user will be notified that a new message is unread in the user list.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/new_message.png}};
        \draw[->] (0, 0) -- (-1.8, 1.7);
        \draw (0.5, 0) node[below] {\footnotesize{\texttt{Number of unread messages}}};
    \end{tikzpicture}
    \caption{Unread messages in the user list}
    \label{fig:new_message}
\end{figure}

\subsection{Sending a file}

To send a file, the user needs to select a conversation by clicking on a \texttt{displayName} in the left user list. The file can be selected by clicking the \texttt{attach} button. The attached file will be sent by clicking the send button.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/attachement_selected.png}};
        \draw[->] (0, 0) -- (-1.5, -3);
        \draw (0.5, 0) node[above] {\footnotesize{\texttt{Attach button}}};
    \end{tikzpicture}
    \caption{Attach button}
    \label{fig:attach_button}
\end{figure}

Clicking on the selected file will show a popup asking the user to confirm the unattachment of the file.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/attachment_delete.png}};
    \end{tikzpicture}
    \caption{Unattach popup}
    \label{fig:unattach popup}
\end{figure}

When receiving a file, clicking on it will prompt the user to choose a download location.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \draw (0, 0) node{\includegraphics[scale=0.4]{images/attachment_sent.png}};
        \draw[->] (1.5, -1) -- (3, 1.35);
        \draw (1.5, -1) node[below] {\footnotesize{\texttt{Click to download}}};
    \end{tikzpicture}
    \caption{Downloading a file}
    \label{fig:file_download}
\end{figure}

\newpage

\section{Software Architecture}

The \texttt{ChatSystem} class is sort of a middleware between the different services and the user interface.

\subsection{Local network communication}

Any local network communications are handled through the \texttt{IndoorUsersService} class. This service contains the logic to detect and notify users on the local network as well as sending and receiving messages between connected users. Local users are refered to as \textit{indoor users}.

In our system, the transit of a message corresponds to opening a \texttt{tcp} communication, sending the message and closing the communication. This is done to reduce the number of open \texttt{tcp} sessions at any given moment, as they have to live in their own thread. This way, the number of running threads corresponds to the number of messages that can be received at the same time.

\subsubsection{Establishing the list of users}

When the \texttt{UserListServer} is started, two things happen :
\begin{itemize}
    \item[1.] A notify message containing our information is broadcasted on the local network to let everyone know that we are connected.
    \item[2.] A list of connected users is then computed by recording any answers to the previous broadcast.
\end{itemize}

In parallel, the \texttt{WaitNotificationThread} waits for any notify messages and answers by sending a \texttt{User} object containing our information. It will also check that the \texttt{displayName} of the user sending the notify isn't already used on the network. If so, the answer will contain a special \texttt{DISPLAYNAME\_ALREADY\_TAKEN} flag.

On the reception of a notify message containing the \texttt{DISCONNECT} flag, the sender will be removed from the list of connected users.

\vspace{1em}
Finally, we assume that we have the complete list of connected users when the time interval between two answers is greater than a second. If no special flags were received, we are connected.

\subsubsection{Receiving messages}

As soon as we have recovered the list of connected users, the \texttt{ReceiveServer} is started. This object listens to any \texttt{tcp} connections on the receive port and forwards them to a thread pool. The task to complete only consists in deserializing the received \texttt{Message} object and calling a callback specified in the \texttt{ChatSystem} with the data.

\subsubsection{Sending messages}

Sending a message on the local network is done through the following steps.
\begin{itemize}
    \item[1.] create a \texttt{Message} object.
    \item[2.] serialize that object to make it ready to be sent.
    \item[3.] open a \texttt{tcp} connection on the recipient's receive port.
    \item[4.] send the raw serialized data.
    \item[5.] close the \texttt{tcp} communication.
\end{itemize}

\subsection{Internet communication}

Internet communications are handled through the \texttt{OutdoorUsersService} class. This service contains the logic to allow any user which isn't wired to the company's local network to still be able to chat with others. Users outside of the company's local network are refered to as \textit{outdoor users}.

We wanted to use a \texttt{websocket} between the server and the users as it would have been better suited for this kind of real-time applications. However we didn't manage to get it working on \texttt{Tomcat}.

An indoor user needs an internet connection in order to chat with outdoor users. If this isn't the case, he will still be able to chat with internal users but will appear disconnected to any outdoor users.

\subsubsection{Servlet paths}

Sending any data to the servlet is achieved with the \texttt{POST} method of the \texttt{http} protocol. To limit the data encapsulation and use the same data structure for both the local and internet communications, no \texttt{type} field could be added to identify if the data sent was for a connection or the sending of a message.

Instead of mapping our servlet to a fixed path such as \texttt{/chat}, we went for a the generic mapping \texttt{/chat/*}. The servlet can then recover the full path and differentiate the types of requests. The paths available on the servlet are as follows.
\begin{itemize}
    \item[-] \texttt{/chat/connect} used to connect to the internet service by sending a \texttt{User} object with our information.
    \item[-] \texttt{/chat/disconnect} used to disconnect to the internet service by sending a \texttt{User} object with our information.
    \item[-] \texttt{/chat/message} used to send a message by streaming a serialized \texttt{Message} object.
    \item[-] \texttt{/chat/displayname} used to update a user's displayName with the linked and updated \texttt{User} object.
\end{itemize}

Special return status codes were defined in order to make the system aware of the state of the request.
\begin{itemize}
    \item[-] \texttt{200} means the request worked.
    \item[-] \texttt{201} means the displayName is already taken.
    \item[-] \texttt{404} means the user was not found in the list of connected users.
    \item[-] \texttt{501} means an error occured on the server while processing the request.
\end{itemize}

\subsubsection{The Update routine}

To compensate for the lack of a \texttt{websocket} support by \texttt{Tomcat}, we implemented an \texttt{update} routine. It is started as soon as the user is connected on the servlet and requests an update with a one second interval.

The request is sent on the \texttt{/chat} path with an \texttt{email} request parameter such as \texttt{/chat?email=<...>}. The servlet answers by sending :
\begin{itemize}
    \item[-] a relevent list of connected users. If the user is indoor, the list only contains the outdoor users whereas an outdoor user will receive the complete list.
    \item[-] a list of new messages for the user. This list is removed from the server as soon as the user has received the update.
\end{itemize}

\subsection{History database}

\texttt{SQlite} is a great way to store the message history, as it is very easy to set up and data can be conveniently recovered with \texttt{SQL} requests. There are two tables in our database (see figure \ref{fig:class_db}).

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \umlclass{MESSAGES}{
            \texttt{+id: text} \\
            \texttt{+source\_mail: text} \\
            \texttt{+dest\_mail: text} \\
            \texttt{+timestamp: timestamp} \\
            \texttt{+text: text} \\
            \texttt{+attachment\_id: text}
        }{}
        \umlclass[x=6]{ATTACHMENTS}{
            \texttt{+id: text} \\
            \texttt{+content: blob}
        }{}
        \umluniassoc[mult1=1, mult2=1]{MESSAGES}{ATTACHMENTS}
    \end{tikzpicture}
    \caption{Class diagram of the database}
    \label{fig:class_db}
\end{figure}

\newpage

The \texttt{SQL} request to retrieve a conversation between \texttt{user1} and \texttt{user2} sorted by timestamp is as follows.

\vspace{1em}
\texttt{SELECT * FROM MESSAGES}

\texttt{WHERE (source\_mail = user1.mail OR source\_mail = user2.mail)}

\texttt{AND (dest\_mail = user1.mail OR dest\_mail = user2.mail)}

\texttt{ORDER BY timestamp}

\subsection{User Interface}

The Graphical User Interface is based upon the \texttt{swing} library, adopting the \texttt{MVC} design pattern.

A \textit{page} of the UI is defined by a view and a controller. The view specifies the look of the page whereas the controller implements its behaviour.
Controllers get updates from the ChatSystem by subscribing to events, feeding a callback. When an event occurs, the ChatSystem handles the events data and runs callbacks from previously subscribed controllers.


\newpage
\section{Known issues}

Here is a list of known bugs that were found but left uncorrected due to a lack of time. A workaround is provided for some of them.

\subsection{Indoor user sometimes doesn't show up in the user list}

This is an error due to the broadcast not reaching the machine. A workaround is to try and connect again.
We measured that this usually happens 1 out of 10 times.

Although we tested the broadcast extensively while implementing the indoor service, this issue started appearing during the integration phase.

\subsection{DisplayName sometimes doesn't update on indoor users}

This is an error due to the broadcast not reaching the machine. A workaround is to try and change the \texttt{displayName} again.
We measured that this usually happens 1 out of 10 times.

Although we tested the broadcast extensively while implementing the indoor service, this issue started appearing during the integration phase.

\subsection{Weird background color in the UI on some hidden elements}

This issue happens when an element is redrawn with \texttt{visibility = false} while the parent's background color isn't the default. The element's footprint on the panel is redrawn with the default background while the parent isn't redrawn.

\newpage
\section{Possible improvements}

Here is a list of improvements to the platform that could be implemented in the future :
\begin{itemize}
    \item[-] Allow the user to read a conversation history with an unconnected user. This is a very easy feature to implement as there is already an \texttt{isConnected} field in the \texttt{User} class as well as a connection indicator in the view.
    \item[-] Include custom message renderers to display sent files in the conversation for certain types such as images or sounds. This would remove the need to save the file before viewing it.
    \item[-] Use a \texttt{websocket} instead of \texttt{http} requests to decrease the amount of unnecessary requests to the servlet.
    \item[-] Add authentication to the project as this is absolutely not secure at the moment.
    \item[-] Replace the local \texttt{sqlite} database with a centralized database in order to have the history available no matter what machine the user is connected on.
\end{itemize}

\newpage
\centering
\addcontentsline{toc}{section}{Appendix}

\includepdf[scale=1.1, pagecommand=\section*{APPENDIX}]{uml/use-case-diagram.pdf}
\includepdf[scale=1, page=1-, pagecommand={}]{uml/class-diagram.pdf}
%\begin{landscape}
\includepdf[scale=1, page=1, landscape=true, pagecommand={}]{uml/sequence-diagrams.pdf}
%\end{landscape}
\includepdf[scale=1, page=2, pagecommand={}]{uml/sequence-diagrams.pdf}


